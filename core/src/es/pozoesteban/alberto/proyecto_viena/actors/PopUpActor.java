package es.pozoesteban.alberto.proyecto_viena.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import es.pozoesteban.alberto.proyecto_viena.utils.AssetsManager;


public class PopUpActor extends ActorDisposable {

    protected Texture texture;
    protected final String texturePath;
    private final TextureRegion[] textureRegions;
    private int xIndex;
    private int yIndex;
    private int numXRegions;
    private int numYRegions;

    public PopUpActor(AssetsManager assetsManager, String texturePath, float widthFactor, float heightFactor, int numXRegions, int numYRegions, int xIndex, int yIndex) {
        super(assetsManager);

        this.texturePath = texturePath;
        this.xIndex = xIndex;
        this.yIndex = yIndex;
        this.numXRegions = numXRegions;
        this.numYRegions = numYRegions;
        textureRegions = new TextureRegion[numXRegions*numYRegions];
        texture = assetsManager.getTexture(texturePath);
        setWidth(Gdx.graphics.getWidth() * widthFactor);
        setHeight(Gdx.graphics.getHeight() * heightFactor);
        setPosition((Gdx.graphics.getWidth()/2) - (getWidth()/2), (Gdx.graphics.getHeight()/2) - (getHeight()/2)); // Centrado
        for (int x = 0; x < numXRegions; x++) {
            for (int y = 0; y < numYRegions; y++) {
                textureRegions[(x*numYRegions) + y] = new TextureRegion(texture, (int)((float)x*(float)texture.getWidth()/(float)numXRegions), (int)((float)y*(float)texture.getHeight()/(float)numYRegions), (int)((float)texture.getWidth()/(float)numXRegions), (int)((float)texture.getHeight()/(float)numYRegions));
            }
        }
    }

    public void changeTexture(int xIndex, int yIndex) {
        this.xIndex = xIndex;
        this.yIndex = yIndex;
    }

    @Override
    public void dispose() {
        assetsManager.disposeAsset(texturePath);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(textureRegions[(xIndex*numYRegions) + yIndex], getX(), getY(), getWidth(), getHeight());
    }
}
