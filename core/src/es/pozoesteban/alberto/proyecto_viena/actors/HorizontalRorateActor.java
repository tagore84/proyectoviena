package es.pozoesteban.alberto.proyecto_viena.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import es.pozoesteban.alberto.proyecto_viena.utils.AssetsManager;

import static es.pozoesteban.alberto.proyecto_viena.games.timeline.screens.TBaseScreen.CIRCULAR_SLIDE_TRAY_SECONDS_MOVE;

public class HorizontalRorateActor extends ActorDisposable {

    private final String textureName;
    private final Texture texture;
    private int position;
    private final float minX;
    private final float width;
    private final float y;
    private final int numElements;

    public HorizontalRorateActor(AssetsManager assetsManager, String textureName, int position, float minX, float maxX, float y, int numElements) {
        super(assetsManager);
        this.textureName = textureName;
        this.texture = assetsManager.getTexture(textureName);
        this.y = y;
        this.minX = minX;
        this.width = maxX - minX;
        this.position = position;
        this.numElements = numElements;
        move();
    }

    @Override
    public void dispose() {
        assetsManager.disposeAsset(textureName);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(texture, getX(), getY(), getWidth(), getHeight());
    }

    public void moveLeft() {
        position--;
        if (position < (numElements/-2)) {
            position = numElements/2;
            setPosition(Gdx.graphics.getWidth(), this.y * 1.2f);
            setSize(0f,0f);
        }
        move();
    }
    public void moveRight() {
        position++;
        if (position > (numElements/2)) {
            position = numElements/-2;
            setPosition(getWidth()*-1, this.y * 1.2f);
            setSize(0f, 0f);
        }
        move();
    }
    private void move() {
        switch (position) {
            case -2:
                addAction(Actions.moveTo(getWidth()*-1, this.y * 1.2f, CIRCULAR_SLIDE_TRAY_SECONDS_MOVE));
                addAction(Actions.sizeTo(0f, 0f, CIRCULAR_SLIDE_TRAY_SECONDS_MOVE));
                break;
            case -1:
                addAction(Actions.moveTo(minX + (width * 0.01f), this.y * 1.2f, CIRCULAR_SLIDE_TRAY_SECONDS_MOVE));
                addAction(Actions.sizeTo(width * 0.27f, width * 0.27f, CIRCULAR_SLIDE_TRAY_SECONDS_MOVE));
                break;
            case 0:
                addAction(Actions.moveTo(minX + (width * 0.31f), this.y, CIRCULAR_SLIDE_TRAY_SECONDS_MOVE));
                addAction(Actions.sizeTo(width * 0.40f, width * 0.40f, CIRCULAR_SLIDE_TRAY_SECONDS_MOVE));
                break;
            case 1:
                addAction(Actions.moveTo(minX + (width * 0.74f), this.y * 1.2f, CIRCULAR_SLIDE_TRAY_SECONDS_MOVE));
                addAction(Actions.sizeTo(width * 0.27f, width * 0.27f, CIRCULAR_SLIDE_TRAY_SECONDS_MOVE));
                break;
            case 2:
                addAction(Actions.moveTo(Gdx.graphics.getWidth(), this.y * 1.2f, CIRCULAR_SLIDE_TRAY_SECONDS_MOVE));
                addAction(Actions.sizeTo(0f, 0f, CIRCULAR_SLIDE_TRAY_SECONDS_MOVE));
                break;
        }
    }
}
