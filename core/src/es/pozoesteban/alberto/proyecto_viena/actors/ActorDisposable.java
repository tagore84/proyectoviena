package es.pozoesteban.alberto.proyecto_viena.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import es.pozoesteban.alberto.proyecto_viena.utils.AssetsManager;

public abstract class ActorDisposable extends Actor {
    protected final AssetsManager assetsManager;
    public ActorDisposable(AssetsManager assetsManager) {
        this.assetsManager = assetsManager;
    }
    public abstract void dispose();

    @Override
    public abstract void draw(Batch batch, float parentAlpha);
}
