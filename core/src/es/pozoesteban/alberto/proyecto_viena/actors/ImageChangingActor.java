package es.pozoesteban.alberto.proyecto_viena.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import es.pozoesteban.alberto.proyecto_viena.utils.AssetsManager;

public class ImageChangingActor extends ActorDisposable {

    private final Texture texture;
    private final TextureRegion[] textureRegions;
    private final float frameDuration;
    private float currentFrameDuration;
    private int frameIndex;
    private final String texturePath;

    public ImageChangingActor(AssetsManager assetsManager, String texturePath, int xPosition, int yPosition, float sizeFactor, int numXRegions, int numYRegions, float frameDuration) {
        this(assetsManager, texturePath, xPosition, yPosition, sizeFactor, 0, numXRegions, numYRegions, frameDuration);
        setHeight(getWidth() * ((float)textureRegions[0].getRegionHeight()/(float)textureRegions[0].getRegionWidth()));
    }
    public ImageChangingActor(AssetsManager assetsManager, String texturePath, int xPosition, int yPosition, float widthFactor, float heightFactor, int numXRegions, int numYRegions, float frameDuration) {
        super(assetsManager);
        this.texturePath = texturePath;
        this.frameDuration = frameDuration;
        this.currentFrameDuration = 0;
        this.frameIndex = 0;
        texture = this.assetsManager.getTexture(texturePath);
        textureRegions = new TextureRegion[numXRegions*numYRegions];
        setX(xPosition);
        setY(yPosition);
        setWidth(Gdx.graphics.getWidth() * widthFactor);
        setHeight(Gdx.graphics.getHeight() * heightFactor);
        for (int x = 0; x < numXRegions; x++) {
            for (int y = 0; y < numYRegions; y++) {
                textureRegions[(x*numYRegions) + y] = new TextureRegion(texture, (int)((float)x*(float)texture.getWidth()/(float)numXRegions), (int)((float)y*(float)texture.getHeight()/(float)numYRegions), (int)((float)texture.getWidth()/(float)numXRegions), (int)((float)texture.getHeight()/(float)numYRegions));
            }
        }

    }

    @Override
    public void act (float delta) {
        currentFrameDuration += delta;
        if (currentFrameDuration > frameDuration) {
            frameIndex = (frameIndex + 1) % textureRegions.length;
            currentFrameDuration = 0;
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(textureRegions[frameIndex], getX(), getY(), getWidth(), getHeight());
    }

    @Override
    public void dispose() {
        assetsManager.disposeAsset(texturePath);
    }
}
