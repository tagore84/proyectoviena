package es.pozoesteban.alberto.proyecto_viena.enums;

public enum Button {

    EXIT(-1),
    DO(0),  RE(1),  MI(2), FA(3),       A(4),  B(5),
    SOL(6), LA(7),  SI(8), DO_PRIME(9), C(10), D(11);

    private final int value;

    Button(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
