package es.pozoesteban.alberto.proyecto_viena.enums;

public enum GamesName {

    GRAND_PIANO_KEYS(0), TIMELINE(1), MOLE_HUNTER(2), SIMON(3), RANDOM_GAME(4);

    private final int value;

    GamesName(int value) {
        this.value = value;
    }
}