package es.pozoesteban.alberto.proyecto_viena;

import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerAdapter;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.controllers.mappings.Xbox;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.proyecto_viena.utils.AssetsManager;
import com.badlogic.gdx.audio.Sound;
import es.pozoesteban.alberto.proyecto_viena.enums.Button;

import java.util.Date;
import java.util.EnumMap;
import java.util.Random;

public abstract class GameController extends ControllerAdapter {

    private final static Logger LOG = new Logger("GameController", Logger.INFO);

    protected MainGame game;

    public static final Random DICE = new Random(new Date().getTime());;


    protected Sound exitTone;
    protected Sound doTone;
    protected Sound reTone;
    protected Sound miTone;
    protected Sound faTone;
    protected Sound solTone;
    protected Sound laTone;
    protected Sound siTone;
    protected Sound doPrimeTone;

    protected Sound errorTone;

    protected Sound doFlatTone;
    protected Sound reFlatTone;

    protected Sound laFlatTone;
    protected Sound siFlatTone;

    protected EnumMap<Button, Boolean> buttonPressed; // DO-0, RE-1, MI-2, FA-3, A-4, B-5 vs SOL-4, LA-7, SI-8, DO_PRIME-9, C-10, D-11

    protected final AssetsManager assetsManager;

    public boolean buttonExitDown(){
        buttonPressed.put(Button.EXIT, true);
        return true;
    }
    public boolean buttonExitUp(){
        buttonPressed.put(Button.EXIT, false);
        return true;
    }

    public boolean buttonDoDown(){
        buttonPressed.put(Button.DO, true);
        return true;
    }
    public boolean buttonReDown(){
        buttonPressed.put(Button.RE, true);
        return true;
    }
    public boolean buttonMiDown(){
        buttonPressed.put(Button.MI, true);
        return true;
    }
    public boolean buttonFaDown(){
        buttonPressed.put(Button.FA, true);
        return true;
    }
    public boolean buttonSolDown(){
        buttonPressed.put(Button.SOL, true);
        return true;
    }
    public boolean buttonLaDown(){
        buttonPressed.put(Button.LA, true);
        return true;
    }
    public boolean buttonSiDown(){
        buttonPressed.put(Button.SI, true);
        return true;
    }
    public boolean buttonDoPrimeDown(){
        buttonPressed.put(Button.DO_PRIME, true);
        return true;
    }
    public boolean buttonADown(){
        buttonPressed.put(Button.A, true);
        return true;
    }
    public boolean buttonBDown(){
        buttonPressed.put(Button.B, true);
        return true;
    }
    public boolean buttonCDown(){
        buttonPressed.put(Button.C, true);
        return true;
    }
    public boolean buttonDDown(){
        buttonPressed.put(Button.D, true);
        return true;
    }

    public boolean buttonDoUp(){
        buttonPressed.put(Button.DO, false);
        return true;
    }
    public boolean buttonReUp(){
        buttonPressed.put(Button.RE, false);
        return true;
    }
    public boolean buttonMiUp(){
        buttonPressed.put(Button.MI, false);
        return true;
    }
    public boolean buttonFaUp(){
        buttonPressed.put(Button.FA, false);
        return true;
    }
    public boolean buttonSolUp(){
        buttonPressed.put(Button.SOL, false);
        return true;
    }
    public boolean buttonLaUp(){
        buttonPressed.put(Button.LA, false);
        return true;
    }
    public boolean buttonSiUp(){
        buttonPressed.put(Button.SI, false);
        return true;
    }
    public boolean buttonDoPrimeUp(){
        buttonPressed.put(Button.DO_PRIME, false);
        return true;
    }
    public boolean buttonAUp(){
        buttonPressed.put(Button.A, false);
        return true;
    }
    public boolean buttonBUp(){
        buttonPressed.put(Button.B, false);
        return true;
    }
    public boolean buttonCUp(){
        buttonPressed.put(Button.C, false);
        return true;
    }
    public boolean buttonDUp(){
        buttonPressed.put(Button.D, false);
        return true;
    }

    protected void loadSounds(){
        exitTone = assetsManager.getSound("sounds/gpk/error.wav"); // ToDo temp sound
    }
    protected void disposeSounds(){
        assetsManager.disposeAsset("sounds/gpk/error.wav"); // ToDo temp sound
    }
    protected void clearTimers() {

    }

    public GameController(MainGame game) {
        this.game = game;
        assetsManager = new AssetsManager();
        buttonPressed = new EnumMap<>(Button.class);
        for(Button button : Button.values()) {
            buttonPressed.put(button, false);
        }

        Controllers.clearListeners();
        Controllers.addListener(this);
        //loadButtonsInputs();
    }
    @Override
    public boolean buttonDown(Controller controller, int buttonCode) {
        int player = Controllers.getControllers().indexOf(controller, false);
        switch (player) {
            case 0:
                switch (buttonCode) {
                    case 0:
                        return buttonExitDown();
                    case 1:
                        return buttonADown();
                    case 2:
                        return buttonBDown();
                    case 3:
                        return buttonDoDown();
                    case 4:
                        return buttonReDown();
                    case 5:
                        return buttonMiDown();
                    case 6:
                        return buttonFaDown();
                    default:
                        return false;
                }
            case 1:
                switch (buttonCode) {
                    case 0:
                        return buttonExitDown();
                    case 1:
                        return buttonCDown();
                    case 2:
                        return buttonDDown();
                    case 3:
                        return buttonSolDown();
                    case 4:
                        return buttonLaDown();
                    case 5:
                        return buttonSiDown();
                    case 6:
                        return buttonDoPrimeDown();
                    default:
                        return false;
                }
            default:
                return false;
        }
    }

    @Override
    public boolean buttonUp(Controller controller, int buttonCode) {
        int player = Controllers.getControllers().indexOf(controller, false);
        switch (player) {
            case 0:
                switch (buttonCode) {
                    case 0:
                        return buttonExitUp();
                    case 1:
                        return buttonAUp();
                    case 2:
                        return buttonBUp();
                    case 3:
                        return buttonDoUp();
                    case 4:
                        return buttonReUp();
                    case 5:
                        return buttonMiUp();
                    case 6:
                        return buttonFaUp();
                    default:
                        return false;
                }
            case 1:
                switch (buttonCode) {
                    case 0:
                        return buttonExitUp();
                    case 1:
                        return buttonCUp();
                    case 2:
                        return buttonDUp();
                    case 3:
                        return buttonSolUp();
                    case 4:
                        return buttonLaUp();
                    case 5:
                        return buttonSiUp();
                    case 6:
                        return buttonDoPrimeUp();
                    default:
                        return false;
                }
            default:
                return false;
        }
    }


    public AssetsManager getAssetsManager() {
        return assetsManager;
    }

    public void backToPrimalUI() {
        game.backToPrimalUI();
    }
    public void backToGame() {
        game.loadSimon();
    }
}
