package es.pozoesteban.alberto.proyecto_viena;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerAdapter;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.GPKController;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.screens.GPKInitialMenuScreen;
import es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.MHController;
import es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.screens.MHInitialMenuScreen;
import es.pozoesteban.alberto.proyecto_viena.games.simon.SimonController;
import es.pozoesteban.alberto.proyecto_viena.games.simon.screens.SimonPlayingScreen;
import es.pozoesteban.alberto.proyecto_viena.games.timeline.TimelineController;
import es.pozoesteban.alberto.proyecto_viena.games.timeline.screens.TSelectNumPlayersScreen;
import es.pozoesteban.alberto.proyecto_viena.screens.PrimalUIScreen;
import es.pozoesteban.alberto.proyecto_viena.screens.SixMassiveButtonsScreen;

public class MainGame extends Game {

	private final static Logger LOG = new Logger("MainGame", Logger.INFO);

	private GameController currentController;



	public void create() {
		LOG.info("Menu Inicial: Primal");
		currentController = new PrimalController(this);
		currentController.loadSounds();
		setScreen(new PrimalUIScreen(currentController));
	}
	public void backToPrimalUI() {
		LOG.info("Back to Primal UI...");
		currentController.clearTimers();
		currentController.disposeSounds();
		currentController = new PrimalController(this);
		currentController.loadSounds();
		setScreen(new PrimalUIScreen(currentController));
	}
	public void loadGrandPianoKeysGame() {
		LOG.info("Loading Grand Piano Keys game...");
		currentController.disposeSounds();
		currentController = new GPKController(this);
		currentController.loadSounds();
		setScreen(new GPKInitialMenuScreen((GPKController)currentController));
	}
	public void loadTimeline() {
		LOG.info("Loading Timeline game...");
		currentController.disposeSounds();
		currentController = new TimelineController(this);
		currentController.loadSounds();
		setScreen(new TSelectNumPlayersScreen((TimelineController)currentController));
	}
	public void loadMoleHunter() {
		LOG.info("Loading Mole Hunter game...");
		currentController.disposeSounds();
		currentController = new MHController(this);
		currentController.loadSounds();
		setScreen(new MHInitialMenuScreen((MHController)currentController));
	}
	public void loadSimon() {
		LOG.info("Loading Simon game...");
		currentController.disposeSounds();
		currentController = new SimonController(this);
		currentController.loadSounds();
		setScreen(new SimonPlayingScreen((SimonController) currentController));
	}

	public void changeScreen(SixMassiveButtonsScreen newScreen) {
		screen.dispose();
		setScreen(newScreen);
	}

	@Override
	public void dispose() {
		super.dispose();
		currentController.disposeSounds();
	}

}
