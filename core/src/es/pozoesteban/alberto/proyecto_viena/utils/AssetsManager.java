package es.pozoesteban.alberto.proyecto_viena.utils;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class AssetsManager {

    private AssetManager manager;

    public AssetsManager() {
        manager = new AssetManager();
        manager.setLoader(Text.class, new TextLoader(new InternalFileHandleResolver()));
    }

    public Texture getTexture(String path) {
        return (Texture) getAsset(path, Texture.class);
    }
    public Sound getSound(String path) {
        return (Sound) getAsset(path, Sound.class);
    }
    private Object getAsset(String path, Class<?> clazz) {
        if(!manager.isLoaded(path)) {
            manager.load(path, clazz);
            manager.finishLoading();
        }
        return manager.get(path, clazz);
    }

    public Skin getSkin(String skinPath) {
        if (!manager.isLoaded(skinPath + ".atlas")) {
            manager.load(skinPath + ".atlas", TextureAtlas.class);
            manager.load(skinPath + ".json", Skin.class, new SkinLoader.SkinParameter(skinPath + ".atlas"));
            manager.finishLoading();
        }
        return manager.get(skinPath + ".json", Skin.class);
    }
    public void disposeAsset(String path) {
        if(manager.isLoaded(path)) manager.unload(path);
    }
    public String getText(String textPath) {
        if(!manager.isLoaded(textPath)) {
            manager.load(new AssetDescriptor<>(textPath, Text.class, new TextLoader.TextParameter()));
            manager.finishLoading();
        }
        return manager.get( textPath, Text.class ).getString();
    }
}
