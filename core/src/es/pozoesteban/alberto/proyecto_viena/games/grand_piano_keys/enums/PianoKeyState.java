package es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.enums;

public enum PianoKeyState {
    UNPRESSABLE(0), PRESSABLE(1), PRESSED(2), ERROR(3);

    private final int value;

    PianoKeyState(int value) {
        this.value = value;
    }
}