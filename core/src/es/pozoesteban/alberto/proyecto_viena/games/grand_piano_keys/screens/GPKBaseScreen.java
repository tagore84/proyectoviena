package es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.screens;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.GPKController;
import es.pozoesteban.alberto.proyecto_viena.screens.SixMassiveButtonsScreen;

public abstract class GPKBaseScreen extends SixMassiveButtonsScreen {

    private final static Logger LOG = new Logger("BaseScreenGrandPianoKeys");

    public static final float KEYS_MOVE_DURATION = 0.15f; // for debug 3
    public static final float KEYS_BY_SCREEN = 3.4f; // for Debug 5f
    public static final float GOOD_JOB_SHOW_TIME = 0.5f;


    public GPKBaseScreen(GPKController controller){
        super(controller);
    }
}