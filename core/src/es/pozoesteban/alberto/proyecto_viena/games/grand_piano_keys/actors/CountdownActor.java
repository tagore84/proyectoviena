package es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import es.pozoesteban.alberto.proyecto_viena.actors.ActorDisposable;
import es.pozoesteban.alberto.proyecto_viena.utils.AssetsManager;

public class CountdownActor extends ActorDisposable {

    protected Texture texture;
    protected TextureRegion[] textureRegions;
    private int currentIndex;

    public CountdownActor(AssetsManager assetsManager, int position, int initIndex, float widthFactor, float heightFactor, int size) {
        super(assetsManager);

        texture = assetsManager.getTexture("textures/cuadricula_8x8_2048_countdown.png");
        setWidth((Gdx.graphics.getWidth()/2) * widthFactor);
        setHeight((Gdx.graphics.getHeight()/2) * heightFactor);
        switch (position) {
            case -1:
                setX((Gdx.graphics.getWidth()/4) - (getWidth()/2)); // Centrado Arriba Player 0
                break;
            case 0:
                setX((Gdx.graphics.getWidth()/2) - (getWidth()/2)); // Centrado Arriba Player 0
                break;
            case 1:
                setX((Gdx.graphics.getWidth()/4 * 3) - (getWidth()/2)); // Centrado Arriba Player 1
                break;
        }
        setY((Gdx.graphics.getHeight()*0.7f) - (getHeight()/2));
        textureRegions = new TextureRegion[size];
        for (int i = 0; i < size; i++) {
            textureRegions[i] = new TextureRegion(texture, (i%8)*256, (i/8)*256, 256, 256);
        }
        currentIndex = initIndex;
    }

    public void changeTextureIndex(int newIndex) {
        currentIndex = newIndex;
    }

    @Override
    public void dispose() {
        assetsManager.disposeAsset("textures/cuadricula_8x8_2048_countdown.png");
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (currentIndex >= 0 && currentIndex < textureRegions.length) batch.draw(textureRegions[currentIndex], getX(), getY(), getWidth(), getHeight());
    }
}
