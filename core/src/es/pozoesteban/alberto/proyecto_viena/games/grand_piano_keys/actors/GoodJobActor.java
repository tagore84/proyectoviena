package es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.Timer;
import es.pozoesteban.alberto.proyecto_viena.actors.ActorDisposable;
import es.pozoesteban.alberto.proyecto_viena.utils.AssetsManager;

import static es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.screens.GPKBaseScreen.GOOD_JOB_SHOW_TIME;

public class GoodJobActor extends ActorDisposable {

    private final static Logger LOG = new Logger("GoodJobActor");

    private final Texture texture;
    private boolean visible;
    private final String texturePath;

    public GoodJobActor(AssetsManager assetsManager, String texturePath, int position) {
        super(assetsManager);
        this.texturePath = texturePath;
        texture = assetsManager.getTexture(texturePath);
        setWidth(Gdx.graphics.getWidth() * 0.2f);
        setHeight(Gdx.graphics.getHeight() * 0.2f);
        switch (position) {
            case -1: // left (Player 1 VsMode)
                setX(Gdx.graphics.getWidth() * 1f/4f - (getWidth()/2f));
                break;
            case 0: // center (Player 1 OnePlayerMode)
                setX(Gdx.graphics.getWidth() * 2f/4f - (getWidth()/2f));
                break;
            case 1: // right (Player 2 VsMode)
                setX(Gdx.graphics.getWidth() * 3f/4f - (getWidth()/2f));
                break;
        }
        setY(Gdx.graphics.getHeight() * 3f/4f);

    }

    @Override
    public void dispose() {
        assetsManager.disposeAsset(texturePath);
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (visible) batch.draw(texture, getX(), getY(), getWidth(), getHeight());
    }

    public void showIt() {
        visible = true;
        Timer.instance().scheduleTask(new Timer.Task() {
            public void run() {
                visible = false;
            }
        }, GOOD_JOB_SHOW_TIME, 0, 0);
    }
}
