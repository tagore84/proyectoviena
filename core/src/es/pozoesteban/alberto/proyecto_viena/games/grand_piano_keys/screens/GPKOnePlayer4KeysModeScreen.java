package es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.screens;

import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.GPKController;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.MassiveKeyActor;

public class GPKOnePlayer4KeysModeScreen extends GPKOnePlayerModeScreen{

    public GPKOnePlayer4KeysModeScreen(GPKController controller) {
        super(controller);
        keys = new MassiveKeyActor[5][4];
    }

    @Override
    protected String getBackgroudTextureName() {
        return "textures/gpk/one_player_4_keys_background.png";
    }
}
