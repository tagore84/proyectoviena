package es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.proyecto_viena.actors.ActorDisposable;
import es.pozoesteban.alberto.proyecto_viena.utils.AssetsManager;

public class ButtonTestActor extends ActorDisposable {

    private final static Logger LOG = new Logger("ButtonTestActor");

    protected Texture activeTexture;
    protected Texture disactiveTexture;
    private boolean pressed;

    public ButtonTestActor(AssetsManager assetsManager, int x, int y) {
        super(assetsManager);

        activeTexture = assetsManager.getTexture("textures/gpk/active_button_test.png");
        disactiveTexture = assetsManager.getTexture("textures/gpk/disactive_button_test.png");
        setWidth(Gdx.graphics.getWidth() / 10f);
        setHeight(Gdx.graphics.getHeight() / 10f);
        setPosition(x, y);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(pressed ? activeTexture : disactiveTexture, getX(), getY(), getWidth(), getHeight());
    }

    @Override
    public void dispose() {
        assetsManager.disposeAsset("textures/gpk/active_button_test.png");
        assetsManager.disposeAsset("textures/gpk/disactive_button_test.png");
    }

    public void press(boolean buttonDown) {
        pressed = buttonDown;
    }
}
