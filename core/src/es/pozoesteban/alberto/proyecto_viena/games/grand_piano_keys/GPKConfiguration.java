package es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys;

public class GPKConfiguration {

    public static final float ERROR_TIME_PENALTY = 2.8f;

    public static final float PLAYER_TIME = 30;

    public static final int WINNING_STREAK_KEYS = 15;

    public static final float WINNING_STREAK_TIME = 1.5f;

}
