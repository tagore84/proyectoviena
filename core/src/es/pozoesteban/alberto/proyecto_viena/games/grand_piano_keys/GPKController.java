package es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.Timer;
import es.pozoesteban.alberto.proyecto_viena.enums.Button;
import es.pozoesteban.alberto.proyecto_viena.GameController;
import es.pozoesteban.alberto.proyecto_viena.MainGame;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.MassiveKeyActor;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.enums.PianoKeyState;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.enums.GPKScreenClass;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.screens.*;

import java.util.Date;
import java.util.Random;

import static es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.GPKConfiguration.*;

public class GPKController extends GameController {

	private static Logger LOG = new Logger("GrandPianoKeysGame", Logger.INFO);

	private int numPlayers;
	private int[][] vsModePatron;
	private int[] onePlayer8KeysPatron;
	private int[] onePlayer4KeysPatron;

	private boolean[] invalidControls; // Player 0, Player 1
	private int countdownNumber;

	private float[] playerTime;

	private int[] currentPosition;
	private int[] winningStreakKeys;


	private int[] keysDone;

	private Sound finish;



	public GPKController(MainGame game) {
		super(game);
		invalidControls = new boolean[2];
		currentPosition = new int[2];
		winningStreakKeys = new int[2];
		keysDone = new int[2];
	}

	private Timer.Task intialCutdown = new Timer.Task() {
		@Override
		public void run() {
			countdownNumber--;
			((GPKPlayingScreen)game.getScreen()).countdown(countdownNumber);
			if (countdownNumber == 0) {
				LOG.info("GO!");
				playerTime = new float[]{PLAYER_TIME, PLAYER_TIME};
				invalidControls = new boolean[]{false, false};
				Timer.schedule(playerTimeTask, 1f, 1f, 10000);
			}
		}
	};

	private Timer.Task playerTimeTask = new Timer.Task() {
		@Override
		public void run() {
			playerTime[0]--;
			if (numPlayers == 2) playerTime[1]--;
			((GPKPlayingScreen)game.getScreen()).updatePlayerTime(playerTime);
			if (playerTime[0] < 0) {
				if (numPlayers == 2) {
					invalidControls[0] = true;
				} else if(numPlayers == 1) {
					invalidControls = new boolean[]{true, true};
				}
			}
			if (numPlayers == 2 && playerTime[1] < 0) {
				invalidControls[1] = true;
			}
			if (playerTime[0] < 0 && (numPlayers == 1 || playerTime[1] < 0)) {
				Timer.instance().clear();
				finish.play();
				LOG.info(("Player One gets " + keysDone[0] + " keys!!! Player Two gets " + keysDone[1] + " keys!!!"));
				((GPKPlayingScreen)game.getScreen()).finishGame(keysDone);
			}
		}
	};


	//<editor-fold defaultstate="collapsed" desc="Controlls">
	@Override
	public boolean buttonExitDown() {
		super.buttonExitDown();
		exitTone.play();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
			case GPKVsModeScreen:
			case GPKOnePlayer4KeysModeScreen:
			case GPKOnePlayer8KeysModeScreen:
			case GPKTestButtonsScreen:
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}

	@Override
	public boolean buttonDoDown() {
		if (invalidControls[0]) return false;
		super.buttonDoDown();
		doTone.play();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.DO, true);
				return true;
			case GPKVsModeScreen:
			case GPKOnePlayer4KeysModeScreen:
			case GPKOnePlayer8KeysModeScreen:
				massiveKeyPressed(0, 0);
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}

	@Override
	public boolean buttonReDown() {
		if (invalidControls[0]) return false;
		super.buttonReDown();
		reTone.play();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.RE, true);
				return true;
			case GPKVsModeScreen:
			case GPKOnePlayer4KeysModeScreen:
			case GPKOnePlayer8KeysModeScreen:
				massiveKeyPressed(1, 0);
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonMiDown() {
		if (invalidControls[0]) return false;
		super.buttonMiDown();
		miTone.play();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.MI, true);
				return true;
			case GPKVsModeScreen:
			case GPKOnePlayer4KeysModeScreen:
			case GPKOnePlayer8KeysModeScreen:
				massiveKeyPressed(2, 0);
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonFaDown() {
		if (invalidControls[0]) return false;
		super.buttonFaDown();
		faTone.play();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.FA, true);
				return true;
			case GPKVsModeScreen:
			case GPKOnePlayer4KeysModeScreen:
			case GPKOnePlayer8KeysModeScreen:
				massiveKeyPressed(3, 0);
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonSolDown() {
		if (invalidControls[1] || (numPlayers == 1 && invalidControls[0])) return false;
		super.buttonSolDown();
		solTone.play();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
			case GPKOnePlayer4KeysModeScreen:
				return true;
			case GPKOnePlayer8KeysModeScreen:
				massiveKeyPressed(4, 0);
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.SOL, true);
				return true;
			case GPKVsModeScreen:
				massiveKeyPressed(0, 1);
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonLaDown() {
		if (invalidControls[1] || (numPlayers == 1 && invalidControls[0])) return false;
		super.buttonLaDown();
		laTone.play();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
			case GPKOnePlayer4KeysModeScreen:
				return true;
			case GPKOnePlayer8KeysModeScreen:
				massiveKeyPressed(5, 0);
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.LA, true);
				return true;
			case GPKVsModeScreen:
				massiveKeyPressed(1, 1);
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonSiDown() {
		if (invalidControls[1] || (numPlayers == 1 && invalidControls[0])) return false;
		super.buttonSiDown();
		siTone.play();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
			case GPKOnePlayer4KeysModeScreen:
				return true;
			case GPKOnePlayer8KeysModeScreen:
				massiveKeyPressed(6, 0);
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.SI, true);
				return true;
			case GPKVsModeScreen:
				massiveKeyPressed(2, 1);
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonDoPrimeDown() {
		if (invalidControls[1] || (numPlayers == 1 && invalidControls[0])) return false;
		super.buttonDoPrimeDown();
		doPrimeTone.play();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
			case GPKOnePlayer4KeysModeScreen:
				return true;
			case GPKOnePlayer8KeysModeScreen:
				massiveKeyPressed(7, 0);
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.DO_PRIME, true);
				return true;
			case GPKVsModeScreen:
				massiveKeyPressed(3, 1);
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonExitUp() {
		super.buttonExitUp();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
				backToPrimalUI();
				return true;
			case GPKTestButtonsScreen:
			case GPKVsModeScreen:
			case GPKOnePlayer8KeysModeScreen:
			case GPKOnePlayer4KeysModeScreen:
				backToMenu();
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonDoUp() {
		super.buttonDoUp();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
				goToOnePlayer8KeysMode();
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.DO, false);
				return true;
			case GPKVsModeScreen:
			case GPKOnePlayer8KeysModeScreen:
			case GPKOnePlayer4KeysModeScreen:
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonReUp() {
		super.buttonReUp();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
				goToOnePlayer4KeysMode();
				return true;
			case GPKVsModeScreen:
			case GPKOnePlayer8KeysModeScreen:
				return true;
			case GPKOnePlayer4KeysModeScreen:
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.RE, false);
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonMiUp() {
		super.buttonMiUp();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
			case GPKVsModeScreen:
			case GPKOnePlayer8KeysModeScreen:
			case GPKOnePlayer4KeysModeScreen:
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.MI, false);
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonFaUp() {
		super.buttonFaUp();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
			case GPKVsModeScreen:
			case GPKOnePlayer8KeysModeScreen:
			case GPKOnePlayer4KeysModeScreen:
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.FA, false);
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonSolUp() {
		super.buttonSolUp();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
				goToVsMode();
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.SOL, false);
				return true;
			case GPKVsModeScreen:
			case GPKOnePlayer8KeysModeScreen:
			case GPKOnePlayer4KeysModeScreen:
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonLaUp() {
		super.buttonLaUp();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
			case GPKVsModeScreen:
			case GPKOnePlayer8KeysModeScreen:
			case GPKOnePlayer4KeysModeScreen:
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.LA, false);
				return true;
			default:
				LOG.error("No screen!");
				return true;
		}
	}
	@Override
	public boolean buttonSiUp() {
		super.buttonSiUp();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
			case GPKVsModeScreen:
			case GPKOnePlayer8KeysModeScreen:
			case GPKOnePlayer4KeysModeScreen:
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.SI, false);
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonDoPrimeUp() {
		super.buttonDoPrimeUp();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
				goToTestButtonsMode();
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.DO_PRIME, false);
				return true;
			case GPKVsModeScreen:
			case GPKOnePlayer8KeysModeScreen:
			case GPKOnePlayer4KeysModeScreen:
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonADown() {
		super.buttonADown();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
			case GPKVsModeScreen:
			case GPKOnePlayer8KeysModeScreen:
			case GPKOnePlayer4KeysModeScreen:
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.A, true);
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonBDown() {
		super.buttonBDown();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
			case GPKVsModeScreen:
			case GPKOnePlayer8KeysModeScreen:
			case GPKOnePlayer4KeysModeScreen:
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.B, true);
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonCDown() {
		super.buttonCDown();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
			case GPKVsModeScreen:
			case GPKOnePlayer8KeysModeScreen:
			case GPKOnePlayer4KeysModeScreen:
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.C, true);
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonDDown() {
		super.buttonDDown();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
			case GPKVsModeScreen:
			case GPKOnePlayer8KeysModeScreen:
			case GPKOnePlayer4KeysModeScreen:
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.D, true);
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonAUp() {
		super.buttonAUp();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
			case GPKVsModeScreen:
			case GPKOnePlayer8KeysModeScreen:
			case GPKOnePlayer4KeysModeScreen:
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.A, false);
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonBUp() {
		super.buttonBUp();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
			case GPKVsModeScreen:
			case GPKOnePlayer8KeysModeScreen:
			case GPKOnePlayer4KeysModeScreen:
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.B, false);
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonCUp() {
		super.buttonCUp();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
			case GPKVsModeScreen:
			case GPKOnePlayer8KeysModeScreen:
			case GPKOnePlayer4KeysModeScreen:
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.C, false);
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	@Override
	public boolean buttonDUp() {
		super.buttonDUp();
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKInitialMenuScreen:
			case GPKVsModeScreen:
			case GPKOnePlayer8KeysModeScreen:
			case GPKOnePlayer4KeysModeScreen:
				return true;
			case GPKTestButtonsScreen:
				((GPKTestButtonsScreen)game.getScreen()).testButton(Button.D, false);
				return true;
			default:
				LOG.error("No screen!");
				return false;
		}
	}
	//</editor-fold

	//<editor-fold defaultstate="collapsed" desc="GoTo...">
	public void goToTestButtonsMode() {
		game.changeScreen(new GPKTestButtonsScreen(this));
	}
	public void goToOnePlayer8KeysMode() {
		this.onePlayer8KeysPatron = createHappyBirthsdayPatron();
		countdownNumber = 3;
		Timer.schedule(intialCutdown, 1f, 1.1f, 3);
		invalidControls = new boolean[]{true, true};
		numPlayers = 1;

		currentPosition = new int[]{0, 0};
		winningStreakKeys = new int[] {0, 0};
		keysDone = new int[]{0, 0};

		game.changeScreen(new GPKOnePlayer8KeysModeScreen(this));
	}
	public void goToOnePlayer4KeysMode() {
		this.onePlayer4KeysPatron = createRandomPatron(4);
		countdownNumber = 3;
		Timer.schedule(intialCutdown, 1f, 1.1f, 3);
		invalidControls = new boolean[]{true, true};
		numPlayers = 1;

		currentPosition = new int[]{0, 0};
		winningStreakKeys = new int[] {0, 0};
		keysDone = new int[]{0, 0};

		game.changeScreen(new GPKOnePlayer4KeysModeScreen(this));
	}
	public void backToMenu() {
		Timer.instance().clear();
		game.changeScreen(new GPKInitialMenuScreen(this));
	}
	public void goToVsMode() {
		int[] patron1P = createRandomPatron(4);
		int[] patron2P = createRandomPatron(4);
		this.vsModePatron = new int[][]{patron1P, patron2P};
		countdownNumber = 3;
		Timer.schedule(intialCutdown, 1f, 1.1f, 3);
		invalidControls = new boolean[]{true, true};
		currentPosition = new int[]{0, 0};
		winningStreakKeys = new int[] {0, 0};
		keysDone = new int[]{0, 0};
		numPlayers = 2;
		game.changeScreen(new GPKVsModeScreen(this));
	}
	//</editor-fold

	//<editor-fold defaultstate="collapsed" desc="Privates...">
	private void massiveKeyPressed(int column, final int player) {
		final MassiveKeyActor buttonPressed = ((GPKPlayingScreen)game.getScreen()).getKey(player, currentPosition[player]%5, column);
		if (getPatron(player)[currentPosition[player]] == column) { //Acierto
			keysDone[player]++;
			if (winningStreakKeys[player] >= (WINNING_STREAK_KEYS-1)) {
				winningStreakKeys[player] = 0;
				playerTime[player] += WINNING_STREAK_TIME;
				((GPKPlayingScreen)game.getScreen()).showGoodJob(player);
			} else {
				winningStreakKeys[player]++;
			}
			currentPosition[player]++;
			((GPKPlayingScreen)game.getScreen()).advanceRow(player, currentPosition[player]);
		} else { // Fallo
			winningStreakKeys[player] = 0;
			invalidControls[player] = true;
			errorTone.play();
			buttonPressed.setState(PianoKeyState.ERROR);
			Timer.instance().scheduleTask(new Timer.Task() {
				public void run() {
					invalidControls[player] = false;
					buttonPressed.setState(PianoKeyState.UNPRESSABLE);
				}
			}, ERROR_TIME_PENALTY, ERROR_TIME_PENALTY, 0);
		}
	}

	private int[] getPatron(int player) {
		switch (GPKScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
			case GPKVsModeScreen:
				return vsModePatron[player];
			case GPKOnePlayer4KeysModeScreen:
				return onePlayer4KeysPatron;
			case GPKOnePlayer8KeysModeScreen:
				return onePlayer8KeysPatron;
		}
		throw new IllegalArgumentException("We dont go to Ravenholm!");
	}

	private int[] createRandomPatron(int numColumns) {
		Random dice = new Random(new Date().getTime());
		int[] patron = new int[300];
		for (int i = 0; i < patron.length; i++) {
			patron[i] = dice.nextInt(numColumns);
		}
		return patron;
	}
	private int[] createDebugPatron(int numColumns) {
		int[] patron = new int[300];
		for (int i = 0; i < patron.length; i++) {
			patron[i] = i % numColumns;
		}
		return patron;
	}

	private int[] createHappyBirthsdayPatron() {
		String[] half = new String[46];
		half[0] = "SOL";
		half[1] = "SOL";
		half[2] = "LA";
		half[3] = "SOL";
		half[4] = "DO";
		half[5] = "SI";
		half[6] = "SOL";
		half[7] = "SOL";
		half[8] = "LA";
		half[9] = "SOL";
		half[10] = "RE";
		half[11] = "DO";
		half[12] = "DO";
		half[13] = "MI";
		half[14] = "SOL";
		half[15] = "MI";
		half[16] = "DO";
		half[17] = "SI";
		half[18] = "LA";
		half[19] = "FA";
		half[20] = "FA";
		half[21] = "MI";
		half[22] = "DO";
		half[23] = "RE";
		half[24] = "DO";
		half[25] = "SOL";
		half[26] = "SOL";
		half[27] = "LA";
		half[28] = "SOL";
		half[29] = "DO";
		half[30] = "SI";
		half[31] = "SOL";
		half[32] = "SOL";
		half[33] = "LA";
		half[34] = "SOL";
		half[35] = "RE";
		half[36] = "DO";
		half[37] = "DO";
		half[38] = "MI";
		half[39] = "SOL";
		half[40] = "MI";
		half[41] = "DO";
		half[42] = "SI";
		half[43] = "LA";
		half[44] = "FA";
		half[45] = "FA";
		return parseMusicSheet(half, 2);
	}

	private int[] parseMusicSheet(String[] piece, int th) {
		int[] output = new int[piece.length*th];
		for (int i = 0; i < output.length; i++) {
			output[i] = parseMusicSheet(piece[i % piece.length]);
		}
		return output;
	}
	private int parseMusicSheet(String note) {
		switch (note.toUpperCase()) {
			case "DO":
				return 0;
			case "RE":
				return 1;
			case "MI":
				return 2;
			case "FA":
				return 3;
			case "SOL":
				return 4;
			case "LA":
				return 5;
			case "SI":
				return 6;
			case "DO_PRIME":
				return 7;
			default:
				return new Random().nextInt(8);
		}
	}
	//</editor-fold>

	@Override
	protected void loadSounds() {
		super.loadSounds();
		errorTone = assetsManager.getSound("sounds/gpk/error.wav");
		doTone = assetsManager.getSound("sounds/gpk/do.mp3");
		reTone = assetsManager.getSound("sounds/gpk/re.mp3");
		miTone = assetsManager.getSound("sounds/gpk/mi.mp3");
		faTone = assetsManager.getSound("sounds/gpk/fa.mp3");
		solTone = assetsManager.getSound("sounds/gpk/sol.mp3");
		laTone = assetsManager.getSound("sounds/gpk/la.mp3");
		siTone = assetsManager.getSound("sounds/gpk/si.mp3");
		doPrimeTone = assetsManager.getSound("sounds/gpk/do_prime.mp3");
		doFlatTone = assetsManager.getSound("sounds/gpk/do_bemol.wav");
		reFlatTone = assetsManager.getSound("sounds/gpk/re_bemol.wav");
		laFlatTone = assetsManager.getSound("sounds/gpk/la_bemol.wav");
		siFlatTone = assetsManager.getSound("sounds/gpk/si_bemol.wav");
		finish = assetsManager.getSound("sounds/gpk/finish.mp3");
	}

	@Override
	protected void disposeSounds() {
		super.disposeSounds();
		assetsManager.disposeAsset("sounds/gpk/error.wav");
		assetsManager.disposeAsset("sounds/gpk/do.mp3");
		assetsManager.disposeAsset("sounds/gpk/re.mp3");
		assetsManager.disposeAsset("sounds/gpk/mi.mp3");
		assetsManager.disposeAsset("sounds/gpk/fa.mp3");
		assetsManager.disposeAsset("sounds/gpk/sol.mp3");
		assetsManager.disposeAsset("sounds/gpk/la.mp3");
		assetsManager.disposeAsset("sounds/gpk/si.mp3");
		assetsManager.disposeAsset("sounds/gpk/do_prime.mp3");
		assetsManager.disposeAsset("sounds/gpk/do_bemol.wav");
		assetsManager.disposeAsset("sounds/gpk/re_bemol.wav");
		assetsManager.disposeAsset("sounds/gpk/la_bemol.wav");
		assetsManager.disposeAsset("sounds/gpk/si_bemol.wav");
		assetsManager.disposeAsset("sounds/gpk/finish.mp3");
	}

	public int[] getVsModePatron(int player) {
		return vsModePatron[player];
	}
	public int[] getOnPlayerPatron(int numKeys) {
		if (numKeys == 4) {
			return onePlayer4KeysPatron;
		} else {
			return onePlayer8KeysPatron;
		}

	}
}
