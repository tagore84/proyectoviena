package es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.proyecto_viena.actors.PopUpActor;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.GPKController;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.BackgroundActor;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.CountdownActor;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.GoodJobActor;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.MassiveKeyActor;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.enums.GPKScreenClass;

import static es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.GPKConfiguration.PLAYER_TIME;

public class GPKVsModeScreen extends GPKPlayingScreen {

    private final static Logger LOG = new Logger("VsModeScreen", Logger.INFO);

    protected MassiveKeyActor[][][] keys;
    private CountdownActor[] countdownActors;

    private GoodJobActor[] goodJobActors;

    private TextArea[] finalPoints;


    public GPKVsModeScreen(GPKController controller) {
        super(controller);
        countdownActors = new CountdownActor[2];
        goodJobActors = new GoodJobActor[2];
        finalPoints = new TextArea[2];
        keys = new MassiveKeyActor[2][5][4]; // 2 Players 5 Rows x 4 Columns (1 hided row)
    }

    public void advanceRow(int player, int currentPosition) {
        for (MassiveKeyActor[] row : keys[player]) {
            for (MassiveKeyActor key : row) {
                key.moveDown(currentPosition);
            }
        }
    }

    @Override
    protected void createActors() {
        backgroundActor = new BackgroundActor(controller.getAssetsManager(), "textures/gpk/vs_background.png");
        allBackgroundActors.add(backgroundActor);
        for (int p = 0; p < keys.length; p++) {
            for (int r = 0; r < keys[p].length; r++) {
                for (int c = 0; c < keys[p][r].length; c++) {
                    MassiveKeyActor key = new MassiveKeyActor(controller.getAssetsManager(), p, r, c, ((GPKController)controller).getVsModePatron(p), GPKScreenClass.valueOf(this.getClass().getSimpleName()));
                    keys[p][r][c] = key;
                    allForegroundActors.add(key);
                }
            }
        }
        countdownActors[0] = new CountdownActor(controller.getAssetsManager(), -1, (int) PLAYER_TIME, 0.2f, 0.2f, (int)(PLAYER_TIME + 1));
        allForegroundActors.add(countdownActors[0]);
        countdownActors[1] = new CountdownActor(controller.getAssetsManager(), 1, (int) PLAYER_TIME, 0.2f, 0.2f, (int)(PLAYER_TIME + 1));
        allForegroundActors.add(countdownActors[1]);

        goodJobActors[0] = new GoodJobActor(controller.getAssetsManager(), "textures/gpk/good_job.png", -1);
        allForegroundActors.add(goodJobActors[0]);
        goodJobActors[1] = new GoodJobActor(controller.getAssetsManager(), "textures/gpk/good_job.png", 1);
        allForegroundActors.add(goodJobActors[1]);

        finalPoints[0] = new TextArea("", skin);
        finalPoints[0].setSize(Gdx.graphics.getWidth() * 0.2f, Gdx.graphics.getHeight() * 0.2f);
        finalPoints[0].setPosition(Gdx.graphics.getWidth() * 1f/4f - (finalPoints[0].getWidth()/2f), Gdx.graphics.getHeight() * 0.5f);
        finalPoints[0].setVisible(false);
        nonDisposablesActors.add(finalPoints[0]);

        finalPoints[1] = new TextArea("", skin);
        finalPoints[1].setSize(Gdx.graphics.getWidth() * 0.2f, Gdx.graphics.getHeight() * 0.2f);
        finalPoints[1].setPosition(Gdx.graphics.getWidth() * 3f/4f - (finalPoints[1].getWidth()/2f), Gdx.graphics.getHeight() * 0.5f);
        finalPoints[1].setVisible(false);
        nonDisposablesActors.add(finalPoints[1]);

        initPopUp = new PopUpActor(controller.getAssetsManager(), "textures/gpk/countdown_2x2.png", 0.2f, 0.2f, 2, 2, 1, 1);
        allForegroundActors.add(initPopUp);

    }

    public MassiveKeyActor getKey(int player, int row, int column) {
        return keys[player][row][column];
    }

    @Override
    public void updatePlayerTime(float[] secondsRemain) {
        countdownActors[0].changeTextureIndex((int)secondsRemain[0]);
        countdownActors[1].changeTextureIndex((int)secondsRemain[1]);
    }

    @Override
    public void showGoodJob(int player) {
        goodJobActors[player].showIt();
    }

    @Override
    public void dispose() {
        super.dispose();
        controller.getAssetsManager().disposeAsset("skins/comiciu/comic-ui");
    }

    @Override
    public void finishGame(int[] keysDone) {
        finalPoints[0].setText("" + keysDone[0] + " Teclas!!!");
        finalPoints[0].setVisible(true);
        finalPoints[1].setText("" + keysDone[1] + " Teclas!!!");
        finalPoints[1].setVisible(true);
    }
}