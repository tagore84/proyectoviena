package es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.proyecto_viena.actors.ImageChangingActor;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.GPKController;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.BackgroundActor;

import static es.pozoesteban.alberto.proyecto_viena.PrimalUIConfiguration.PREVIEW_SECONDS_CHANGE;


public class GPKInitialMenuScreen extends GPKBaseScreen {

    private final static Logger LOG = new Logger("InitialMenuScreen");

    private ImageChangingActor onePlayer8KeysMode;
    private ImageChangingActor onePlayer4KeysMode;
    private ImageChangingActor vsMode;

    public GPKInitialMenuScreen(GPKController controller) {
        super(controller);
        disposeActors();
        initArrays();
    }

    @Override
    public void show() {
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        stage.setDebugAll(SCREEN_DEBUG);

        createActors();
        addActorsToStage();
        loadInputs();
    }

    @Override
    protected void createActors() {
        backgroundActor = new BackgroundActor(controller.getAssetsManager(), "textures/gpk/initial_menu_background.png");
        allBackgroundActors.add(backgroundActor);


        onePlayer8KeysMode = new ImageChangingActor(controller.getAssetsManager(), "textures/gpk/one_player_8_keys.png",
                50 + (0 * Gdx.graphics.getWidth()/9), Gdx.graphics.getWidth()/4,
                0.1f, 2, 2, PREVIEW_SECONDS_CHANGE);
        allForegroundActors.add(onePlayer8KeysMode);
        onePlayer4KeysMode = new ImageChangingActor(controller.getAssetsManager(), "textures/gpk/one_player_4_keys.png",
                50 + (1 * Gdx.graphics.getWidth()/9), Gdx.graphics.getWidth()/4,
                0.1f, 2, 2, PREVIEW_SECONDS_CHANGE);
        allForegroundActors.add(onePlayer4KeysMode);
        vsMode = new ImageChangingActor(controller.getAssetsManager(), "textures/gpk/vs_mode.png",
                120 + (4 * Gdx.graphics.getWidth()/9), Gdx.graphics.getWidth()/4,
                0.1f, 2, 2, PREVIEW_SECONDS_CHANGE);
        allForegroundActors.add(vsMode);
    }

}
