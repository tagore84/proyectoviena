package es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.proyecto_viena.enums.Button;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.GPKController;
import es.pozoesteban.alberto.proyecto_viena.actors.ActorDisposable;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.BackgroundActor;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.ButtonTestActor;

public class GPKTestButtonsScreen extends GPKBaseScreen {

    private final static Logger LOG = new Logger("TestButtonsScreen");

    private ActorDisposable backgroundActor;

    private ButtonTestActor doButtonTest; // A
    private ButtonTestActor aButtonTest; // W
    private ButtonTestActor reButtonTest; // S
    private ButtonTestActor bButtonTest; // E
    private ButtonTestActor miButtonTest; // D
    private ButtonTestActor faButtonTest; // F

    private ButtonTestActor solButtonTest; // G
    private ButtonTestActor laButtonTest; // H
    private ButtonTestActor cButtonTest; // I
    private ButtonTestActor siButtonTest; // J
    private ButtonTestActor dButtonTest; // O
    private ButtonTestActor doPrimeButtonTest; // K

    private ButtonTestActor smallLeftButtonTest; // T
    private ButtonTestActor smallRightButtonTest; // Y

    public GPKTestButtonsScreen(GPKController controller) {
        super(controller);
        disposeActors();
        initArrays();
    }

    @Override
    public void show() {
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        stage.setDebugAll(SCREEN_DEBUG);

        createActors();
        addActorsToStage();
        loadInputs();
    }

    public void testButton(Button pressed, boolean buttonDown) {
        switch (pressed) {
            case DO:
                doButtonTest.press(buttonDown);
                break;
            case RE:
                reButtonTest.press(buttonDown);
                break;
            case MI:
                miButtonTest.press(buttonDown);
                break;
            case FA:
                faButtonTest.press(buttonDown);
                break;
            case SOL:
                solButtonTest.press(buttonDown);
                break;
            case LA:
                laButtonTest.press(buttonDown);
                break;
            case SI:
                siButtonTest.press(buttonDown);
                break;
            case DO_PRIME:
                doPrimeButtonTest.press(buttonDown);
                break;
            case A:
                aButtonTest.press(buttonDown);
                break;
            case B:
                bButtonTest.press(buttonDown);
                break;
            case C:
                cButtonTest.press(buttonDown);
                break;
            case D:
                dButtonTest.press(buttonDown);
                break;
        }
    }

    @Override
    protected void createActors() {
        backgroundActor = new BackgroundActor(controller.getAssetsManager(), "textures/gpk/test_buttons_background.png");
        allBackgroundActors.add(backgroundActor);

        doButtonTest = new ButtonTestActor(controller.getAssetsManager(), (int) (10 + (0f*(Gdx.graphics.getWidth()/10f))), (int) (10 + (1f*(Gdx.graphics.getHeight()/6f))));
        allForegroundActors.add(doButtonTest);

        aButtonTest = new ButtonTestActor(controller.getAssetsManager(), (int) (10 + (0.5f*(Gdx.graphics.getWidth()/10f))), (int) (10 + (2f*(Gdx.graphics.getHeight()/6f))));
        allForegroundActors.add(aButtonTest);

        reButtonTest = new ButtonTestActor(controller.getAssetsManager(), (int) (10 + (1f*(Gdx.graphics.getWidth()/10f))), (int) (10 + (1f*(Gdx.graphics.getHeight()/6f))));
        allForegroundActors.add(reButtonTest);

        bButtonTest = new ButtonTestActor(controller.getAssetsManager(), (int) (10 + (1.5f*(Gdx.graphics.getWidth()/10f))), (int) (10 + (2f*(Gdx.graphics.getHeight()/6f))));
        allForegroundActors.add(bButtonTest);

        miButtonTest = new ButtonTestActor(controller.getAssetsManager(), (int) (10 + (2f*(Gdx.graphics.getWidth()/10f))), (int) (10 + (1f*(Gdx.graphics.getHeight()/6f))));
        allForegroundActors.add(miButtonTest);

        faButtonTest = new ButtonTestActor(controller.getAssetsManager(), (int) (10 + (3f*(Gdx.graphics.getWidth()/10f))), (int) (10 + (1f*(Gdx.graphics.getHeight()/6f))));
        allForegroundActors.add(faButtonTest);


        solButtonTest = new ButtonTestActor(controller.getAssetsManager(), (int) (10 + (6f*(Gdx.graphics.getWidth()/10f))), (int) (10 + (1f*(Gdx.graphics.getHeight()/6f))));
        allForegroundActors.add(solButtonTest);

        laButtonTest = new ButtonTestActor(controller.getAssetsManager(), (int) (10 + (7f*(Gdx.graphics.getWidth()/10f))), (int) (10 + (1f*(Gdx.graphics.getHeight()/6f))));
        allForegroundActors.add(laButtonTest);

        cButtonTest = new ButtonTestActor(controller.getAssetsManager(), (int) (10 + (7.5f*(Gdx.graphics.getWidth()/10f))), (int) (10 + (2f*(Gdx.graphics.getHeight()/6f))));
        allForegroundActors.add(cButtonTest);

        siButtonTest = new ButtonTestActor(controller.getAssetsManager(), (int) (10 + (8f*(Gdx.graphics.getWidth()/10f))), (int) (10 + (1f*(Gdx.graphics.getHeight()/6f))));
        allForegroundActors.add(siButtonTest);

        dButtonTest = new ButtonTestActor(controller.getAssetsManager(), (int) (10 + (8.5f*(Gdx.graphics.getWidth()/10f))), (int) (10 + (2f*(Gdx.graphics.getHeight()/6f))));
        allForegroundActors.add(dButtonTest);

        doPrimeButtonTest = new ButtonTestActor(controller.getAssetsManager(), (int) (10 + (9f*(Gdx.graphics.getWidth()/10f))), (int) (10 + (1f*(Gdx.graphics.getHeight()/6f))));
        allForegroundActors.add(doPrimeButtonTest);


        smallLeftButtonTest = new ButtonTestActor(controller.getAssetsManager(), (int) (10 + (4f*(Gdx.graphics.getWidth()/10f))), (int) (10 + (2f*(Gdx.graphics.getHeight()/6f))));
        allForegroundActors.add(smallLeftButtonTest);
        smallRightButtonTest = new ButtonTestActor(controller.getAssetsManager(), (int) (10 + (5f*(Gdx.graphics.getWidth()/10f))), (int) (10 + (2f*(Gdx.graphics.getHeight()/6f))));
        allForegroundActors.add(smallRightButtonTest);
    }

}
