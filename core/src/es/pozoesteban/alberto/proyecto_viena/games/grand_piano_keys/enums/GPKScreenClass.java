package es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.enums;

public enum GPKScreenClass {
    GPKInitialMenuScreen(0), GPKTestButtonsScreen(1), GPKVsModeScreen(2), GPKOnePlayer8KeysModeScreen(3), GPKOnePlayer4KeysModeScreen(4);

    private final int value;

    GPKScreenClass(int value) {
        this.value = value;
    }
}
