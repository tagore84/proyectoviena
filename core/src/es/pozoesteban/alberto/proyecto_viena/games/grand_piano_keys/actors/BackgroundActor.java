package es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.proyecto_viena.actors.ActorDisposable;
import es.pozoesteban.alberto.proyecto_viena.utils.AssetsManager;

public class BackgroundActor extends ActorDisposable {

    private final static Logger LOG = new Logger("BackgroundActor");

    protected Texture texture;
    protected final String texturePath;

    public BackgroundActor(AssetsManager assetsManager, String texturePath) {
        super(assetsManager);
        this.texturePath = texturePath;
        texture = assetsManager.getTexture(texturePath);
        setWidth(Gdx.graphics.getWidth());
        setHeight(Gdx.graphics.getHeight());
        setPosition(0, 0);
    }
    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(texture, getX(), getY(), getWidth(), getHeight());
    }

    public void dispose() {
        assetsManager.disposeAsset(texturePath);
    }
}
