package es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.proyecto_viena.actors.ActorDisposable;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.enums.PianoKeyState;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.enums.GPKScreenClass;
import es.pozoesteban.alberto.proyecto_viena.utils.AssetsManager;

import static es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.screens.GPKBaseScreen.KEYS_BY_SCREEN;
import static es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.screens.GPKBaseScreen.KEYS_MOVE_DURATION;

public class MassiveKeyActor extends ActorDisposable {

    private final static Logger LOG = new Logger("MassiveKeyActor");

    protected Texture pressableTexture;
    protected Texture unpressableTexture;
    protected Texture pressedTexture;
    protected Texture errorTexture;

    private PianoKeyState state;

    private final int player;
    private int row;
    private final boolean[] patron;

    public MassiveKeyActor(AssetsManager assetsManager, int player, int row, int column, int[] patron, GPKScreenClass GPKScreenClass) {
        super(assetsManager);
        this.patron = new boolean[patron.length];
        for (int i = 0; i < patron.length; i++) {
            this.patron[i] = patron[i] == column;
        }
        this.row = row;
        this.player = player;
        this.state = this.patron[row] ? PianoKeyState.PRESSABLE : PianoKeyState.UNPRESSABLE;

        pressableTexture = player <= 0 ? assetsManager.getTexture("textures/gpk/pressable_1p.png")
                : assetsManager.getTexture("textures/gpk/pressable_2p.png");
        unpressableTexture = assetsManager.getTexture("textures/gpk/unpressable.png");
        pressedTexture = assetsManager.getTexture("textures/gpk/pressed.png");
        errorTexture = assetsManager.getTexture("textures/gpk/error.png");

        setHeight(Gdx.graphics.getHeight() / KEYS_BY_SCREEN);

        switch (player) {
            case 0:
                setWidth((Gdx.graphics.getWidth() * 0.98f) / 8f);
                setPosition(((Gdx.graphics.getWidth() * 0.97f) / 8f) * column, (Gdx.graphics.getHeight() / KEYS_BY_SCREEN) * row);
                break;
            case 1:
                setWidth((Gdx.graphics.getWidth() * 0.98f) / 8f);
                setPosition((Gdx.graphics.getWidth() * 0.51f) + ((Gdx.graphics.getWidth() * 0.98f) / 8f) * column, (Gdx.graphics.getHeight() / KEYS_BY_SCREEN) * row);
                break;
            case -1:
                if (GPKScreenClass == GPKScreenClass.GPKOnePlayer8KeysModeScreen) {
                    setWidth((Gdx.graphics.getWidth() * 1f) / 8f);
                    setPosition(((Gdx.graphics.getWidth() * 1f) / 8f) * column, (Gdx.graphics.getHeight() / KEYS_BY_SCREEN) * row);
                } else {
                    setWidth((Gdx.graphics.getWidth() * 1f) / 8f);
                    setPosition(((Gdx.graphics.getWidth() * 1f) / 8f) * (column + 2), (Gdx.graphics.getHeight() / KEYS_BY_SCREEN) * row);
                }
                break;
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        switch (state) {
            case PRESSABLE:
                batch.draw(pressableTexture, getX(), getY(), getWidth(), getHeight());
                break;
            case UNPRESSABLE:
                batch.draw(unpressableTexture, getX(), getY(), getWidth(), getHeight());
                break;
            case PRESSED:
                batch.draw(pressedTexture, getX(), getY(), getWidth(), getHeight());
                break;
            case ERROR:
                batch.draw(errorTexture, getX(), getY(), getWidth(), getHeight());
                break;
        }
    }

    @Override
    public void dispose() {
        if (player <= 0) {
            assetsManager.disposeAsset("textures/gpk/pressable_1p.png");
        } else {
            assetsManager.disposeAsset("textures/gpk/pressable_2p.png");
        }
        assetsManager.disposeAsset("textures/gpk/unpressable.png");
        assetsManager.disposeAsset("textures/gpk/pressed.png");
        assetsManager.disposeAsset("textures/gpk/error.png");
    }
    private int mod(int a, int b) {
        int c = a % b;
        return (c < 0) ? c + b : c;
    }
    public void moveDown(int currentRow) {
        int currentPosition = mod((row - currentRow), 5);
        if (patron[currentRow-1] && currentPosition == 4) { // Blue/Green Button
            setState(PianoKeyState.PRESSED);
        } else { // White Button

        }
        float newYPosition = currentPosition == 4 ? (Gdx.graphics.getHeight() / KEYS_BY_SCREEN) * -1 : (Gdx.graphics.getHeight() / KEYS_BY_SCREEN) * currentPosition;
        if (currentPosition == 3) {// Primero hay que subirla para que después baje...
            setY((Gdx.graphics.getHeight() / KEYS_BY_SCREEN) * 4);
            setState(patron[currentRow+3] ? PianoKeyState.PRESSABLE : PianoKeyState.UNPRESSABLE);
        }
        getActions().clear();
        addAction(Actions.moveTo(getX(), newYPosition, KEYS_MOVE_DURATION));
    }
    public void setState(PianoKeyState state) {
        this.state = state;
    }
}