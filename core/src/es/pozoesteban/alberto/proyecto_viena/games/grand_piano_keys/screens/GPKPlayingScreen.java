package es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import es.pozoesteban.alberto.proyecto_viena.actors.ActorDisposable;
import es.pozoesteban.alberto.proyecto_viena.actors.PopUpActor;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.GPKController;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.MassiveKeyActor;

public abstract class GPKPlayingScreen extends GPKBaseScreen {

    protected ActorDisposable backgroundActor;

    protected PopUpActor initPopUp;

    protected Skin skin;


    public GPKPlayingScreen(GPKController controller) {
        super(controller);
        disposeActors();
        initArrays();
    }

    @Override
    public void show() {
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        stage.setDebugAll(SCREEN_DEBUG);

        skin = new Skin(Gdx.files.internal("skins/uiskin/uiskin.json"));
        skin.getFont("default-font").getData().setScale(2f, 2f);

        createActors();
        addActorsToStage();

        loadInputs();
    }

    public void countdown(int countdownNumber) {
        if (countdownNumber >= 0) {
            initPopUp.changeTexture(countdownNumber%2, countdownNumber/2);
        } else {
            initPopUp.setVisible(false);
            initPopUp.dispose();
        }
    }

    public abstract void advanceRow(int player, int currentPosition);
    public abstract void showGoodJob(int player);

    protected abstract void createActors();

    public abstract void updatePlayerTime(float[] secondsRemain);

    public abstract void finishGame(int[] keysDone);

    public abstract MassiveKeyActor getKey(int player, int row, int column);
}
