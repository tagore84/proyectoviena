package es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import es.pozoesteban.alberto.proyecto_viena.actors.PopUpActor;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.GPKController;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.BackgroundActor;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.CountdownActor;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.GoodJobActor;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.MassiveKeyActor;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.enums.GPKScreenClass;

import static es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.GPKConfiguration.PLAYER_TIME;

public abstract class GPKOnePlayerModeScreen extends GPKPlayingScreen {

    protected CountdownActor countdownActor;
    protected GoodJobActor goodJobActor;
    protected TextArea finalPoints;

    protected MassiveKeyActor[][] keys;

    public GPKOnePlayerModeScreen(GPKController controller) {
        super(controller);
    }

    @Override
    public void updatePlayerTime(float[] secondsRemain) {
        countdownActor.changeTextureIndex((int)secondsRemain[0]);
    }

    @Override
    public void finishGame(int[] keysDone) {
        finalPoints.setText("" + keysDone[0] + " Teclas!!!");
        finalPoints.setVisible(true);
    }

    @Override
    public MassiveKeyActor getKey(int player, int row, int column) {
        return keys[row][column];
    }

    @Override
    public void showGoodJob(int player) {
        goodJobActor.showIt();
    }

    @Override
    public void advanceRow(int player, int currentPosition) {
        for (MassiveKeyActor[] row : keys) {
            for (MassiveKeyActor key : row) {
                key.moveDown(currentPosition);
            }
        }
    }

    @Override
    protected void createActors() {
        backgroundActor = new BackgroundActor(controller.getAssetsManager(), getBackgroudTextureName());
        allBackgroundActors.add(backgroundActor);

        for (int r = 0; r < keys.length; r++) {
            for (int c = 0; c < keys[r].length; c++) {
                MassiveKeyActor key = new MassiveKeyActor(controller.getAssetsManager(), -1, r, c, ((GPKController)controller).getOnPlayerPatron(keys[0].length), GPKScreenClass.valueOf(this.getClass().getSimpleName()));
                keys[r][c] = key;
                allForegroundActors.add(key);
            }
        }
        goodJobActor = new GoodJobActor(controller.getAssetsManager(), "textures/gpk/good_job.png", 0);
        allForegroundActors.add(goodJobActor);

        countdownActor = new CountdownActor(controller.getAssetsManager(), 0, (int) PLAYER_TIME, 0.2f, 0.2f, (int)(PLAYER_TIME + 1));
        allForegroundActors.add(countdownActor);

        finalPoints = new TextArea("", skin);
        finalPoints.setSize(Gdx.graphics.getWidth() * 0.2f, Gdx.graphics.getHeight() * 0.2f);
        finalPoints.setPosition(Gdx.graphics.getWidth() * 2f/4f - (finalPoints.getWidth()/2f), Gdx.graphics.getHeight() * 0.5f);
        finalPoints.setVisible(false);
        nonDisposablesActors.add(finalPoints);

        initPopUp = new PopUpActor(controller.getAssetsManager(), "textures/gpk/countdown_2x2.png", 0.2f, 0.2f, 2, 2, 1, 1);
        allForegroundActors.add(initPopUp);
    }

    protected abstract String getBackgroudTextureName();
}
