package es.pozoesteban.alberto.proyecto_viena.games.timeline.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import es.pozoesteban.alberto.proyecto_viena.actors.ImageChangingActor;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.BackgroundActor;
import es.pozoesteban.alberto.proyecto_viena.games.timeline.TimelineController;

import static es.pozoesteban.alberto.proyecto_viena.PrimalUIConfiguration.PREVIEW_SECONDS_CHANGE;

public class TSelectNumPlayersScreen extends TBaseScreen {

    private ImageChangingActor[] numPlayersActors;

    public TSelectNumPlayersScreen(TimelineController controller) {
        super(controller);
        disposeActors();
        initArrays();
        numPlayersActors = new ImageChangingActor[8];
    }

    @Override
    public void show() {
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        stage.setDebugAll(SCREEN_DEBUG);

        createActors();
        addActorsToStage();
        loadInputs();
    }
    @Override
    protected void createActors() {
        backgroundActor = new BackgroundActor(controller.getAssetsManager(), "textures/timeline/initial_menu_background.png");
        allBackgroundActors.add(backgroundActor);

        for (int i = 0; i < 4; i++) {
            numPlayersActors[i] = new ImageChangingActor(controller.getAssetsManager(), "textures/timeline/num_players_" + (i + 1) + ".png",
                    50 + (i * Gdx.graphics.getWidth() / 9), Gdx.graphics.getWidth() / 4,
                    0.1f, 2, 2, PREVIEW_SECONDS_CHANGE);
            allForegroundActors.add(numPlayersActors[i]);
        }
        for (int i = 4; i < numPlayersActors.length; i++) {
            numPlayersActors[i] = new ImageChangingActor(controller.getAssetsManager(), "textures/timeline/num_players_" + (i + 1) + ".png",
                    120 + (i * Gdx.graphics.getWidth() / 9), Gdx.graphics.getWidth() / 4,
                    0.1f, 2, 2, PREVIEW_SECONDS_CHANGE);
            allForegroundActors.add(numPlayersActors[i]);
        }

    }

}
