package es.pozoesteban.alberto.proyecto_viena.games.timeline.screens;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.proyecto_viena.games.timeline.TimelineController;
import es.pozoesteban.alberto.proyecto_viena.screens.SixMassiveButtonsScreen;

public abstract class TBaseScreen extends SixMassiveButtonsScreen {

    private final static Logger LOG = new Logger("BaseScreenGrandPianoKeys");

    public static final float CIRCULAR_SLIDE_TRAY_SECONDS_MOVE = 0.3f; // for debug 3


    public TBaseScreen(TimelineController controller) {
        super(controller);
    }

}