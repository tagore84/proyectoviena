package es.pozoesteban.alberto.proyecto_viena.games.timeline.enums;

public enum TScreenClass {
    TSelectNumPlayersScreen(0), TPlayingScreen(1);

    private final int value;

    TScreenClass(int value) {
        this.value = value;
    }
}

