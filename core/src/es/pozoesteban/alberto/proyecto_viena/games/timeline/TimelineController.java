package es.pozoesteban.alberto.proyecto_viena.games.timeline;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.proyecto_viena.GameController;
import es.pozoesteban.alberto.proyecto_viena.MainGame;
import es.pozoesteban.alberto.proyecto_viena.enums.Button;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.enums.GPKScreenClass;
import es.pozoesteban.alberto.proyecto_viena.games.timeline.enums.TScreenClass;
import es.pozoesteban.alberto.proyecto_viena.games.timeline.model.TimelineEvent;
import es.pozoesteban.alberto.proyecto_viena.games.timeline.screens.TPlayingScreen;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class TimelineController extends GameController {

    private static Logger LOG = new Logger("TimelineController", Logger.INFO);

    private final Random dice;

    private ArrayList<TimelineEvent> timelineEvents;

    private int numPlayers;

    public TimelineController(MainGame game) {
        super(game);
        timelineEvents = new ArrayList();
        dice = new Random(new Date().getTime());
        JSONArray timelineArray = new JSONArray(getAssetsManager().getText("data/timeline/timeline.json"));
        for (Object o : timelineArray) {
            JSONObject json = (JSONObject) o;
            timelineEvents.add(new TimelineEvent(json));
        }
    }

    @Override
    protected void loadSounds() {
        super.loadSounds();
        // ToDo load sounds...
    }

    @Override
    protected void disposeSounds() {
        super.disposeSounds();
        // ToDo dispose sounds...
    }

    //<editor-fold defaultstate="collapsed" desc="Controlls">
    @Override
    public boolean buttonExitDown() {
        super.buttonExitDown();
        exitTone.play();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
            case TPlayingScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonDoDown() {
        super.buttonDoDown();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
            case TPlayingScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonReDown() {
        super.buttonReDown();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
            case TPlayingScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonMiDown() {
        super.buttonMiDown();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
            case TPlayingScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonFaDown() {
        super.buttonFaDown();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
            case TPlayingScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonSolDown() {
        super.buttonSolDown();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
            case TPlayingScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonLaDown() {
        super.buttonLaDown();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
            case TPlayingScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonSiDown() {
        super.buttonSiDown();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
            case TPlayingScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonDoPrimeDown() {
        super.buttonDoPrimeDown();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
            case TPlayingScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }

    @Override
    public boolean buttonExitUp() {
        super.buttonExitUp();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
            case TPlayingScreen:
                backToPrimalUI();
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonDoUp() {
        super.buttonDoUp();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
                goToPlayingMode(1);
                return true;
            case TPlayingScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonReUp() {
        super.buttonReUp();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
                goToPlayingMode(2);
                return true;
            case TPlayingScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonMiUp() {
        super.buttonMiUp();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
                goToPlayingMode(3);
                return true;
            case TPlayingScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonFaUp() {
        super.buttonFaUp();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
                goToPlayingMode(4);
                return true;
            case TPlayingScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonSolUp() {
        super.buttonSolUp();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
                goToPlayingMode(5);
                return true;
            case TPlayingScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonLaUp() {
        super.buttonLaUp();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
                goToPlayingMode(6);
                return true;
            case TPlayingScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonSiUp() {
        super.buttonSiUp();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
                goToPlayingMode(7);
                return true;
            case TPlayingScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonDoPrimeUp() {
        super.buttonDoPrimeUp();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
                goToPlayingMode(8);
                return true;
            case TPlayingScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonAUp() {
        super.buttonAUp();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
            case TPlayingScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonBUp() {
        super.buttonBUp();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
            case TPlayingScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonCUp() {
        super.buttonCUp();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
            case TPlayingScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonDUp() {
        super.buttonDUp();
        switch (TScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case TSelectNumPlayersScreen:
            case TPlayingScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    //</editor-fold>

    //goToPlayingMode(1);
    private void goToPlayingMode(int numPlayers) {
        this.numPlayers = numPlayers;
        game.changeScreen(new TPlayingScreen(this));
    }

    public TimelineEvent nextTimelineEvent() {
        if (timelineEvents.isEmpty()) throw new IllegalArgumentException("Timeline events is empty!");
        int index = dice.nextInt(timelineEvents.size());
        TimelineEvent selected = timelineEvents.get(index);
        timelineEvents.remove(index);
        return selected;
    }
}
