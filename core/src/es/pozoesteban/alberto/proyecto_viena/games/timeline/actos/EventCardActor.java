package es.pozoesteban.alberto.proyecto_viena.games.timeline.actos;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import es.pozoesteban.alberto.proyecto_viena.GameController;
import es.pozoesteban.alberto.proyecto_viena.actors.ActorDisposable;
import es.pozoesteban.alberto.proyecto_viena.games.timeline.model.TimelineEvent;
import es.pozoesteban.alberto.proyecto_viena.utils.AssetsManager;

public class EventCardActor extends ActorDisposable {

    private final TimelineEvent event;
    private final Texture texture;

    public EventCardActor(AssetsManager assetsManager, TimelineEvent event, int position) {
        super(assetsManager);
        this.event = event;
        this.texture = assetsManager.getTexture(event.getTextureName());
        float margin = Gdx.graphics.getWidth() * 0.05f;
        float totalWith = Gdx.graphics.getWidth() - (2 * margin);
        setWidth(totalWith/8.8f);
        setHeight(getWidth() * 1.8f);
        if (position == -1) {
            setPosition((Gdx.graphics.getWidth()/2) - (getWidth()/2), Gdx.graphics.getHeight() * 0.05f);
        } else {
            setY(Gdx.graphics.getHeight() * 0.8f - getHeight());
            setX(margin + (((totalWith-getWidth()) / 4f ) * position));
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(texture, getX(), getY(), getWidth(), getHeight());
    }

    @Override
    public void dispose() {
        assetsManager.disposeAsset(event.getTextureName());
    }
}
