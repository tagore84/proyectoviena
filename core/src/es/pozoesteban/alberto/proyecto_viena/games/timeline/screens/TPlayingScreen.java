package es.pozoesteban.alberto.proyecto_viena.games.timeline.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.GPKConfiguration;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.BackgroundActor;
import es.pozoesteban.alberto.proyecto_viena.games.timeline.TimelineController;
import es.pozoesteban.alberto.proyecto_viena.games.timeline.actos.EventCardActor;

import java.util.ArrayList;
import java.util.List;

public class TPlayingScreen extends TBaseScreen {

    private final static Logger LOG = new Logger("TPlayingScreen", Logger.INFO);

    private final List<EventCardActor> cards;
    private EventCardActor currentCard;

    public TPlayingScreen(TimelineController controller) {
        super(controller);
        disposeActors();
        initArrays();
        cards = new ArrayList();
    }

    public void show() {
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        stage.setDebugAll(SCREEN_DEBUG);

        createActors();
        addActorsToStage();

        loadInputs();
    }

    @Override
    protected void createActors() {
        backgroundActor = new BackgroundActor(controller.getAssetsManager(), "textures/timeline/playing_background.png");
        allBackgroundActors.add(backgroundActor);

        currentCard = new EventCardActor(controller.getAssetsManager(), ((TimelineController)controller).nextTimelineEvent(), -1);
        allForegroundActors.add(currentCard);

        // ToDo remove...
        for (int i = 0; i < 5; i++) {
            currentCard = new EventCardActor(controller.getAssetsManager(), ((TimelineController)controller).nextTimelineEvent(), i);
            allForegroundActors.add(currentCard);
        }

    }


}
