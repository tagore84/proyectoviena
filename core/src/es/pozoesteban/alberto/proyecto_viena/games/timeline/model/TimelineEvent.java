package es.pozoesteban.alberto.proyecto_viena.games.timeline.model;

import org.json.*;

public class TimelineEvent {

    private final int id;
    private final String name;
    private final int year;
    private final int month;
    private final int day;
    private final String textureName;

    public TimelineEvent(JSONObject json) {
        id = json.getInt("id");
        name = json.getString("name");
        year = json.getInt("year");
        month = json.getInt("month");
        day = json.getInt("day");
        textureName = json.getString("texture_name");
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public String getTextureName() {
        return textureName;
    }
}
