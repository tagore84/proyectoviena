package es.pozoesteban.alberto.proyecto_viena.games.simon.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import es.pozoesteban.alberto.proyecto_viena.actors.ActorDisposable;
import es.pozoesteban.alberto.proyecto_viena.games.simon.enums.SimonColor;
import es.pozoesteban.alberto.proyecto_viena.utils.AssetsManager;

public class SimonButtonActor extends ActorDisposable {

    private final String textureNameOn;
    private final String textureNameOff;
    private final Texture on;
    private final Texture off;

    private boolean state;

    public SimonButtonActor(AssetsManager assetsManager, SimonColor color) {
        super(assetsManager);
        switch (color) {
            case GREEN:
                this.textureNameOn = "textures/button_green.png";
                break;
            case RED:
                this.textureNameOn = "textures/button_red.png";
                break;
            case YELLOW:
                this.textureNameOn = "textures/button_yellow.png";
                break;
            case BLUE:
                this.textureNameOn = "textures/button_blue.png";
                break;
            default:
                throw new IllegalArgumentException("Invalid color " + color);
        }
        this.textureNameOff = "textures/button_grey.png";
        on = assetsManager.getTexture(textureNameOn);
        off = assetsManager.getTexture(textureNameOff);
        setSize(Gdx.graphics.getWidth() * 0.2f, Gdx.graphics.getWidth() * 0.2f);
        switch (color.getValue()) {
            case 0:
                setPosition(Gdx.graphics.getWidth() / 2f - (getWidth() * 1.02f), Gdx.graphics.getHeight() / 2f +  (getWidth() * 0.02f));
                break;
            case 1:
                setPosition(Gdx.graphics.getWidth() / 2f + (getWidth() * 0.02f), Gdx.graphics.getHeight() / 2f +  (getWidth() * 0.02f));
                break;
            case 2:
                setPosition(Gdx.graphics.getWidth() / 2f + (getWidth() * 0.02f), Gdx.graphics.getHeight() / 2f -  (getWidth() * 1.02f));
                break;
            case 3:
                setPosition(Gdx.graphics.getWidth() / 2f - (getWidth() * 1.02f), Gdx.graphics.getHeight() / 2f -  (getWidth() * 1.02f));
                break;
        }
    }

    public void on() {
        state = true;
    }
    public void off() {
        state = false;
    }

    @Override
    public void dispose() {
        assetsManager.disposeAsset(textureNameOff);
        assetsManager.disposeAsset(textureNameOn);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(state ? on : off, getX(), getY(), getWidth(), getHeight());
    }
}
