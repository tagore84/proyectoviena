package es.pozoesteban.alberto.proyecto_viena.games.simon.enums;

public enum SimonColor {
    GREEN(0), RED(1), YELLOW(2), BLUE(3);

    private final int value;

    SimonColor(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}