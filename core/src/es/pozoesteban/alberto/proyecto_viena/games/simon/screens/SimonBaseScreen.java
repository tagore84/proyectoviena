package es.pozoesteban.alberto.proyecto_viena.games.simon.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import es.pozoesteban.alberto.proyecto_viena.games.simon.SimonController;
import es.pozoesteban.alberto.proyecto_viena.screens.SixMassiveButtonsScreen;

public abstract class SimonBaseScreen extends SixMassiveButtonsScreen {


    public SimonBaseScreen(SimonController controller) {
        super(controller);
    }

    @Override
    public void show() {
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        stage.setDebugAll(SCREEN_DEBUG);

        createActors();
        addActorsToStage();

        loadInputs();
    }

}