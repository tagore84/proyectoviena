package es.pozoesteban.alberto.proyecto_viena.games.simon.enums;

public enum SimonScreenClass {
    SimonPlayingScreen(0), SimonGameOverScreen(1);

    private final int value;

    SimonScreenClass(int value) {
        this.value = value;
    }
}
