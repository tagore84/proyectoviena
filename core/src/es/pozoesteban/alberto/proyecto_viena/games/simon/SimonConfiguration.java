package es.pozoesteban.alberto.proyecto_viena.games.simon;

public class SimonConfiguration {

    public static final float INIT_WAIT_TIME = 1f;

    public static final float LIGHT_ON_TIME = 0.4f;

    public static final float LIGHT_OFF_TIME = 0.1f;

}
