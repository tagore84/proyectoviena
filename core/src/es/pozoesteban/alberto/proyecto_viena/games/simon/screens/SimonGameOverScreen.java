package es.pozoesteban.alberto.proyecto_viena.games.simon.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.BackgroundActor;
import es.pozoesteban.alberto.proyecto_viena.games.simon.SimonController;
import es.pozoesteban.alberto.proyecto_viena.games.simon.actors.SimonButtonActor;

public class SimonGameOverScreen extends SimonBaseScreen {

    protected TextArea finalPoints;

    protected Skin skin;

    public SimonGameOverScreen(SimonController controller) {
        super(controller);
        disposeActors();
        initArrays();


    }


    @Override
    protected void createActors() {
        backgroundActor = new BackgroundActor(controller.getAssetsManager(), "textures/simon/gameover_background.png");
        allBackgroundActors.add(backgroundActor);

        skin = new Skin(Gdx.files.internal("skins/uiskin/uiskin.json"));
        skin.getFont("default-font").getData().setScale(2f, 2f);

        finalPoints = new TextArea("" + ((SimonController)controller).getPoints() + " Rondas!!!", skin);
        finalPoints.setSize(Gdx.graphics.getWidth() * 0.2f, Gdx.graphics.getHeight() * 0.2f);
        finalPoints.setPosition(Gdx.graphics.getWidth() * 2f/4f - (finalPoints.getWidth()/2f), Gdx.graphics.getHeight() * 0.5f);
        finalPoints.setVisible(true);
        nonDisposablesActors.add(finalPoints);

    }


}
