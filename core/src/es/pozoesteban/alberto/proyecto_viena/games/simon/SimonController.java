package es.pozoesteban.alberto.proyecto_viena.games.simon;

import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.Timer;
import es.pozoesteban.alberto.proyecto_viena.GameController;
import es.pozoesteban.alberto.proyecto_viena.MainGame;
import es.pozoesteban.alberto.proyecto_viena.games.simon.enums.SimonColor;
import es.pozoesteban.alberto.proyecto_viena.games.simon.enums.SimonScreenClass;
import es.pozoesteban.alberto.proyecto_viena.games.simon.screens.SimonGameOverScreen;
import es.pozoesteban.alberto.proyecto_viena.games.simon.screens.SimonPlayingScreen;

import static es.pozoesteban.alberto.proyecto_viena.games.simon.SimonConfiguration.*;

public class SimonController extends GameController {

    private static Logger LOG = new Logger("SimonController", Logger.INFO);

    private boolean listen;
    private int currentRound;
    private int currentColorSwitchingOn;
    private int currentColorSwitchingOff;
    private int currentColorPushin;

    private int countdownNumber;

    private final SimonColor[][] patron;

    private final Timer initial;
    private final Timer lightOnTimer;
    private final Timer lightOffTimer;

    public SimonController(MainGame game) {
        super(game);
        this.patron = createRandomPatron(100);
        countdownNumber = 3;
        currentRound = 0;
        currentColorSwitchingOn = 0;
        currentColorSwitchingOff = 0;
        currentColorPushin = 0;
        listen = true;

        initial = new Timer();
        initial.schedule(intialCutdown, 1f, 1.1f, 5);

        lightOnTimer = new Timer();
        lightOffTimer = new Timer();
    }

    private Timer.Task intialCutdown = new Timer.Task() {
        @Override
        public void run() {
            if (game.getScreen().getClass() != SimonPlayingScreen.class) return;
            countdownNumber--;
            ((SimonPlayingScreen)game.getScreen()).countdown(countdownNumber);
            if (countdownNumber == 0) {
                LOG.info("GO!");
                listen = true;
                lightOnTimer.schedule(simonSays, INIT_WAIT_TIME, 0, 0);
            }
        }
    };
    private Timer.Task simonSays = new Timer.Task() {
        @Override
        public void run() {
            logPatron();
            lightOnTimer.scheduleTask(lightOnNextButton, INIT_WAIT_TIME, 0f, 0);
        }
    };

    private void logPatron() {
        StringBuilder sb = new StringBuilder();
        sb.append("Round ").append(currentRound).append(" :");
        for (SimonColor color : patron[currentRound]) {
            sb.append(color.name() + " ");
        }
        LOG.info(sb.toString());
    }

    private Timer.Task lightOnNextButton = new Timer.Task() {
        @Override
        public void run() {
            if (currentColorSwitchingOn < patron[currentRound].length) {
                SimonColor currentColor = patron[currentRound][currentColorSwitchingOn];
                tone(currentColor);
                ((SimonPlayingScreen)game.getScreen()).lightOn(currentColor);
                lightOffTimer.scheduleTask(lightOffNextButton, LIGHT_ON_TIME, 0f, 0);
                currentColorSwitchingOn++;
                lightOnTimer.scheduleTask(lightOnNextButton, LIGHT_ON_TIME + LIGHT_OFF_TIME, 0f, 0);
            }
        }
    };
    private Timer.Task lightOffNextButton = new Timer.Task() {
        @Override
        public void run() {
            ((SimonPlayingScreen)game.getScreen()).lightOff(patron[currentRound][currentColorSwitchingOff]);
            currentColorSwitchingOff++;
            if (currentColorSwitchingOff >= patron[currentRound].length) {
                listen = false;
            }
        }
    };

    @Override
    protected void loadSounds() {
        super.loadSounds();
        errorTone = assetsManager.getSound("sounds/simon/error.wav");
        doTone = assetsManager.getSound("sounds/simon/green.mp3");
        reTone = assetsManager.getSound("sounds/simon/red.mp3");
        miTone = assetsManager.getSound("sounds/simon/yellow.mp3");
        faTone = assetsManager.getSound("sounds/simon/blue.mp3");
    }

    private void push(SimonColor color) {
        ((SimonPlayingScreen)game.getScreen()).lightOff(color);
        if (patron[currentRound][currentColorPushin++] == color) {
            if (currentColorPushin >= patron[currentRound].length) {
                currentRound++;
                currentColorPushin = 0;
                currentColorSwitchingOff = 0;
                currentColorSwitchingOn = 0;
                listen = true;
                lightOnTimer.schedule(simonSays, 1f, 1f, 0);
            }
        } else {
            errorTone.play();
            goToSimonGameOverScreen();
        }
    }

    private void tone(SimonColor color) {
        switch (color) {
            case GREEN:
                doTone.play();
                break;
            case RED:
                faTone.play();
                break;
            case BLUE:
                reTone.play();
                break;
            case YELLOW:
                miTone.play();
                break;
        }
    }

    private void goToSimonGameOverScreen() {
        initial.clear();
        lightOnTimer.clear();
        lightOffTimer.clear();
        game.changeScreen(new SimonGameOverScreen(this));
    }

    protected void clearTimers() {
        initial.stop();
        initial.clear();
        lightOnTimer.stop();
        lightOnTimer.clear();
        lightOffTimer.stop();
        lightOffTimer.clear();
    }


    @Override
    protected void disposeSounds() {
        super.disposeSounds();
        assetsManager.disposeAsset("sounds/simon/error.wav");
        assetsManager.disposeAsset("sounds/simon/green.mp3");
        assetsManager.disposeAsset("sounds/simon/red.mp3");
        assetsManager.disposeAsset("sounds/simon/yellow.mp3");
        assetsManager.disposeAsset("sounds/simon/blue.mp3");
    }

    public int getPoints() {
        return currentRound;
    }

    //<editor-fold defaultstate="collapsed" desc="Controlls">
    @Override
    public boolean buttonExitDown() {
        super.buttonExitDown();
        exitTone.play();
        switch (SimonScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case SimonPlayingScreen:
            case SimonGameOverScreen:
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonReDown() {
        super.buttonReDown();
        switch (SimonScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case SimonPlayingScreen:
                if (!listen) {
                    ((SimonPlayingScreen)game.getScreen()).lightOn(SimonColor.BLUE);
                    reTone.play();
                }
                return !listen;
            case SimonGameOverScreen:
                return false;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonMiDown() {
        super.buttonMiDown();
        switch (SimonScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case SimonPlayingScreen:
                if (!listen) {
                    ((SimonPlayingScreen)game.getScreen()).lightOn(SimonColor.YELLOW);
                    miTone.play();
                }
                return !listen;
            case SimonGameOverScreen:
                return false;
            default:
                LOG.error("No screen!");
                return false;
        }
    }

    @Override
    public boolean buttonADown() {
        super.buttonADown();
        switch (SimonScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case SimonPlayingScreen:
                if (!listen) {
                    ((SimonPlayingScreen)game.getScreen()).lightOn(SimonColor.GREEN);
                    doTone.play();
                }
                return !listen;
            case SimonGameOverScreen:
                return false;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonBDown() {
        super.buttonBDown();
        switch (SimonScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case SimonPlayingScreen:
                if (!listen) {
                    ((SimonPlayingScreen)game.getScreen()).lightOn(SimonColor.RED);
                    faTone.play();
                }
                return !listen;
            case SimonGameOverScreen:
                return false;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonExitUp() {
        super.buttonExitUp();
        switch (SimonScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case SimonPlayingScreen:
                backToPrimalUI();
                return true;
            case SimonGameOverScreen:
                backToGame();
                return true;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonReUp() {
        super.buttonReUp();
        switch (SimonScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case SimonPlayingScreen:
                if (!listen) push(SimonColor.BLUE);
                return true;
            case SimonGameOverScreen:
                return false;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonMiUp() {
        super.buttonMiUp();
        switch (SimonScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case SimonPlayingScreen:
                if (!listen) push(SimonColor.YELLOW);
                return true;
            case SimonGameOverScreen:
                return false;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonAUp() {
        super.buttonAUp();
        switch (SimonScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case SimonPlayingScreen:
                if (!listen) push(SimonColor.GREEN);
                return true;
            case SimonGameOverScreen:
                return false;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    @Override
    public boolean buttonBUp() {
        super.buttonBUp();
        switch (SimonScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case SimonPlayingScreen:
                if (!listen) push(SimonColor.RED);
                return true;
            case SimonGameOverScreen:
                return false;
            default:
                LOG.error("No screen!");
                return false;
        }
    }
    //</editor-fold>


    private SimonColor[][] createRandomPatron(int size) {
        SimonColor[][] output = new SimonColor[size][];
        output[0] = new SimonColor[1];
        output[0][0] = SimonColor.values()[DICE.nextInt(4)];
        for (int i = 1; i < 100; i++) {
            output[i] = new SimonColor[i+1];
            for (int j = 0; j < i; j++) {
                output[i][j] = output[i-1][j];
            }
            output[i][i] =  SimonColor.values()[DICE.nextInt(4)];
        }
        return output;
    }


}
