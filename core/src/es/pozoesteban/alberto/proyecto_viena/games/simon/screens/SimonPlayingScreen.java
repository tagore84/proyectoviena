package es.pozoesteban.alberto.proyecto_viena.games.simon.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import es.pozoesteban.alberto.proyecto_viena.actors.PopUpActor;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.BackgroundActor;
import es.pozoesteban.alberto.proyecto_viena.games.simon.SimonController;
import es.pozoesteban.alberto.proyecto_viena.games.simon.actors.SimonButtonActor;
import es.pozoesteban.alberto.proyecto_viena.games.simon.enums.SimonColor;

public class SimonPlayingScreen extends SimonBaseScreen {

    private SimonButtonActor[] buttons;

    protected PopUpActor initPopUp;


    public SimonPlayingScreen(SimonController controller) {
        super(controller);
        disposeActors();
        initArrays();
        buttons = new SimonButtonActor[4];
    }

    @Override
    protected void createActors() {
        backgroundActor = new BackgroundActor(controller.getAssetsManager(), "textures/simon/background.png");
        allBackgroundActors.add(backgroundActor);

        buttons[0] = new SimonButtonActor(controller.getAssetsManager(), SimonColor.GREEN);
        allForegroundActors.add(buttons[0]);
        buttons[1] = new SimonButtonActor(controller.getAssetsManager(), SimonColor.RED);
        allForegroundActors.add(buttons[1]);
        buttons[2] = new SimonButtonActor(controller.getAssetsManager(), SimonColor.YELLOW);
        allForegroundActors.add(buttons[2]);
        buttons[3] = new SimonButtonActor(controller.getAssetsManager(), SimonColor.BLUE);
        allForegroundActors.add(buttons[3]);

        initPopUp = new PopUpActor(controller.getAssetsManager(), "textures/simon/countdown_2x2.png", 0.2f, 0.2f, 2, 2, 1, 1);
        allForegroundActors.add(initPopUp);
    }

    public void lightOn(SimonColor color) {
        buttons[color.getValue()].on();
    }
    public void lightOff(SimonColor color) {
        buttons[color.getValue()].off();
    }

    public void countdown(int countdownNumber) {
        if (countdownNumber >= 0) {
            initPopUp.changeTexture(countdownNumber%2, countdownNumber/2);
        } else {
            initPopUp.setVisible(false);
            initPopUp.dispose();
        }
    }
}
