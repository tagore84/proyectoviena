package es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.enums;

public enum MHModes {

    ONE_PLAYER_12_MOLES(0), ONE_PLAYER_6_MOLES(1), TWO_PLAYERS(2);

    private final int value;

    MHModes(int value) {
        this.value = value;
    }
}