package es.pozoesteban.alberto.proyecto_viena.games.mole_hunter;

public class MHConfiguration {

    public static final float DOUBLE_MOLE_LIKELIHOOD = 0.2f;
    public static final int PLAYER_TIME = 30;

    public static final float MOLE_MIN_HIDE_TIME = 1f;
    public static final float MOLE_MAX_HIDE_TIME = 3f;

    public static final float MOLE_MIN_UNHIDE_TIME = 1f;
    public static final float MOLE_MAX_UNHIDE_TIME = 2f;

    public static final float MOLE_TIME_GO_UP = 0.8f;
    public static final float MOLE_TIME_GO_DOWN = 0.5f;
    public static final float MOLE_TIME_GO_DOWN_PUNCHED = 0.2f;

    public static final float PENALTY_MISTAKE_TIME = 2.5f;
}
