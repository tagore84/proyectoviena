package es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.MHController;
import es.pozoesteban.alberto.proyecto_viena.screens.SixMassiveButtonsScreen;

public abstract class MHBaseScreen extends SixMassiveButtonsScreen {


    public MHBaseScreen(MHController controller) {
        super(controller);
    }

    @Override
    public void show() {
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        stage.setDebugAll(SCREEN_DEBUG);

        createActors();
        addActorsToStage();

        loadInputs();
    }

    protected abstract String getBackgroudTextureName();
}