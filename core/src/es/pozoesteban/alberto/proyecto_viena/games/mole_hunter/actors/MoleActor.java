package es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import es.pozoesteban.alberto.proyecto_viena.actors.ActorDisposable;
import es.pozoesteban.alberto.proyecto_viena.enums.Button;
import es.pozoesteban.alberto.proyecto_viena.utils.AssetsManager;

import static es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.MHConfiguration.*;

public class MoleActor extends ActorDisposable {

    private final Texture normalTexture;
    private final Texture punchedTexture;
    private final String normalTextureName;
    private final String punchedTextureName;
    private float baseY;

    private boolean isPunched;

    public MoleActor(AssetsManager assetsManager, String normalTextureName, String punchedTextureName, float widthFactor, float heightFactor, Button position) {
        super(assetsManager);
        this.isPunched = false;
        this.normalTextureName = normalTextureName;
        this.punchedTextureName = punchedTextureName;
        this.normalTexture = assetsManager.getTexture(normalTextureName);
        this.punchedTexture = assetsManager.getTexture(punchedTextureName);
        setSize(Gdx.graphics.getWidth() * widthFactor, Gdx.graphics.getWidth() * heightFactor);
        switch (position) {
            case DO: case RE: case MI: case FA: case SOL: case LA: case SI: case DO_PRIME:
                baseY = Gdx.graphics.getHeight() * 0.3f;
                break;
            case A: case B: case C: case D:
                baseY = Gdx.graphics.getHeight() * 0.6f;
                break;
        }
        setY(baseY);
        switch (position) {
            case DO:
                setX(Gdx.graphics.getWidth() * 0.02f);
                break;
            case RE:
                setX(Gdx.graphics.getWidth() * 0.12f);
                break;
            case MI:
                setX(Gdx.graphics.getWidth() * 0.22f);
                break;
            case FA:
                setX(Gdx.graphics.getWidth() * 0.32f);
                break;
            case SOL:
                setX(Gdx.graphics.getWidth() * 0.52f);
                break;
            case LA:
                setX(Gdx.graphics.getWidth() * 0.62f);
                break;
            case SI:
                setX(Gdx.graphics.getWidth() * 0.72f);
                break;
            case DO_PRIME:
                setX(Gdx.graphics.getWidth() * 0.82f);
                break;
            case A:
                setX(Gdx.graphics.getWidth() * 0.12f);
                break;
            case B:
                setX(Gdx.graphics.getWidth() * 0.22f);
                break;
            case C:
                setX(Gdx.graphics.getWidth() * 0.62f);
                break;
            case D:
                setX(Gdx.graphics.getWidth() * 0.72f);
                break;
        }
    }

    public void punchMole() {
        isPunched = true;
        addAction(Actions.moveTo(getX(), baseY, MOLE_TIME_GO_DOWN_PUNCHED));
    }
    public void unpunchMole() {
        isPunched = false;
    }
    public void showUp() {
        addAction(Actions.moveTo(getX(), baseY + (getHeight()*0.48f), MOLE_TIME_GO_UP));
    }

    public void showDown() {
        addAction(Actions.moveTo(getX(), baseY, MOLE_TIME_GO_DOWN));
    }

    @Override
    public void dispose() {
        assetsManager.disposeAsset(normalTextureName);
        assetsManager.disposeAsset(punchedTextureName);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(isPunched ? punchedTexture : normalTexture, getX(), getY(), getWidth(), getHeight());
    }
}
