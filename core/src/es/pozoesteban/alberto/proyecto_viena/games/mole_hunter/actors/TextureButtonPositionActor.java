package es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import es.pozoesteban.alberto.proyecto_viena.actors.ActorDisposable;
import es.pozoesteban.alberto.proyecto_viena.enums.Button;
import es.pozoesteban.alberto.proyecto_viena.utils.AssetsManager;

public class TextureButtonPositionActor extends ActorDisposable {

    private final String textureName;
    private final Texture texture;

    public TextureButtonPositionActor(AssetsManager assetsManager, String textureName, float widthFactor, float heightFactor, Button position) {
        super(assetsManager);
        this.textureName = textureName;
        this.texture = assetsManager.getTexture(textureName);
        setSize(Gdx.graphics.getWidth() * widthFactor, Gdx.graphics.getWidth() * heightFactor);
        switch (position) {
            case DO: case RE: case MI: case FA: case SOL: case LA: case SI: case DO_PRIME:
                setY(Gdx.graphics.getHeight() * 0.3f);
                break;
            case A: case B: case C: case D:
                setY(Gdx.graphics.getHeight() * 0.6f);
                break;
        }
        switch (position) {
            case DO:
                setX(Gdx.graphics.getWidth() * 0.02f);
                break;
            case RE:
                setX(Gdx.graphics.getWidth() * 0.12f);
                break;
            case MI:
                setX(Gdx.graphics.getWidth() * 0.22f);
                break;
            case FA:
                setX(Gdx.graphics.getWidth() * 0.32f);
                break;
            case SOL:
                setX(Gdx.graphics.getWidth() * 0.52f);
                break;
            case LA:
                setX(Gdx.graphics.getWidth() * 0.62f);
                break;
            case SI:
                setX(Gdx.graphics.getWidth() * 0.72f);
                break;
            case DO_PRIME:
                setX(Gdx.graphics.getWidth() * 0.82f);
                break;
            case A:
                setX(Gdx.graphics.getWidth() * 0.12f);
                break;
            case B:
                setX(Gdx.graphics.getWidth() * 0.22f);
                break;
            case C:
                setX(Gdx.graphics.getWidth() * 0.62f);
                break;
            case D:
                setX(Gdx.graphics.getWidth() * 0.72f);
                break;
        }
    }

    @Override
    public void dispose() {
        assetsManager.disposeAsset(textureName);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(texture, getX(), getY(), getWidth(), getHeight());
    }
}
