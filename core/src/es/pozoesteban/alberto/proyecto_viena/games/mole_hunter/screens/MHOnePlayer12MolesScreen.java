package es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.screens;

import es.pozoesteban.alberto.proyecto_viena.actors.PopUpActor;
import es.pozoesteban.alberto.proyecto_viena.enums.Button;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.CountdownActor;
import es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.MHController;
import es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.actors.MoleActor;
import es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.actors.TextureButtonPositionActor;
import es.pozoesteban.alberto.proyecto_viena.games.simon.SimonController;

import static es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.GPKConfiguration.PLAYER_TIME;

public class MHOnePlayer12MolesScreen extends MHPlayingScreen {

    protected CountdownActor countdownActor;

    public MHOnePlayer12MolesScreen(MHController controller) {
        super(controller);
    }

    @Override
    protected String getBackgroudTextureName() {
        return "textures/mole_hunter/one_player_12_moles_background.png";
    }

    @Override
    protected void createActors() {
        super.createActors();

        allForegroundActors.addAll(createLeftMoles());
        allForegroundActors.addAll(createRightMoles());

        countdownActor = new CountdownActor(controller.getAssetsManager(), 0, (int) PLAYER_TIME, 0.2f, 0.2f, (int)(PLAYER_TIME + 1));
        allForegroundActors.add(countdownActor);

        initPopUp = new PopUpActor(controller.getAssetsManager(), "textures/mole_hunter/countdown_2x2.png", 0.2f, 0.2f, 2, 2, 1, 1);
        allForegroundActors.add(initPopUp);

    }
    @Override
    public void updatePlayerTime(float[] secondsRemain) {
        countdownActor.changeTextureIndex((int)secondsRemain[0]);
    }

}
