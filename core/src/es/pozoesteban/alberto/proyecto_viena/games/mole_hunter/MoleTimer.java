package es.pozoesteban.alberto.proyecto_viena.games.mole_hunter;

import com.badlogic.gdx.utils.Timer;
import es.pozoesteban.alberto.proyecto_viena.enums.Button;
import es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.screens.MHPlayingScreen;

import java.util.Map;

import static es.pozoesteban.alberto.proyecto_viena.GameController.DICE;
import static es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.MHConfiguration.*;

public class MoleTimer {

    private boolean isUp;

    public MoleTimer(final MHPlayingScreen screen, final Button position) {
        isUp = true;
        float timeUp = MOLE_MIN_UNHIDE_TIME + (DICE.nextFloat() * (MOLE_MAX_UNHIDE_TIME - MOLE_MIN_UNHIDE_TIME));
        screen.moleGoUp(position);
        Timer goDownTimer = new Timer();
        final Timer isDownTimer = new Timer();
        goDownTimer.schedule(new Timer.Task() {
            @Override
            public void run() {
                screen.moleGoDown(position);
                isDownTimer.scheduleTask(new Timer.Task() {
                    @Override
                    public void run() {
                        isUp = false;
                        screen.unpunchMole(position);
                    }}, MOLE_TIME_GO_DOWN, 0, 0);
            }}, timeUp, 0, 0);
    }

    public boolean isUp() {
        return isUp;
    }

    public void isUp(boolean b) {
        isUp = b;
    }
}
