package es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.screens;

import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.proyecto_viena.actors.ActorDisposable;
import es.pozoesteban.alberto.proyecto_viena.actors.PopUpActor;
import es.pozoesteban.alberto.proyecto_viena.enums.Button;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.BackgroundActor;
import es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.MHController;
import es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.actors.MoleActor;
import es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.actors.TextureButtonPositionActor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class MHPlayingScreen extends MHBaseScreen {

    private static Logger LOG = new Logger("MHPlayingScreen", Logger.INFO);

    protected Map<Button, MoleActor> moles;
    protected ActorDisposable backgroundActor;
    protected PopUpActor initPopUp;

    private float sizeFactor = 0.1f;

    public MHPlayingScreen(MHController controller) {
        super(controller);
        disposeActors();
        initArrays();
        moles = new HashMap();
    }


    @Override
    protected void createActors() {
        backgroundActor = new BackgroundActor(controller.getAssetsManager(), getBackgroudTextureName());
        allBackgroundActors.add(backgroundActor);
    }

    protected List<ActorDisposable> createLeftMoles() {
        List<ActorDisposable> moleActors = new ArrayList();
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_background.png", sizeFactor, sizeFactor, Button.DO));
        MoleActor current = new MoleActor(controller.getAssetsManager(), "textures/mole_hunter/mole.png", "textures/mole_hunter/mole_hunted.png", sizeFactor, sizeFactor, Button.DO);
        moles.put(Button.DO, current);
        allForegroundActors.add(current);
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_foreground.png", sizeFactor, sizeFactor, Button.DO));
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_background.png", sizeFactor, sizeFactor, Button.RE));
        current = new MoleActor(controller.getAssetsManager(), "textures/mole_hunter/mole.png", "textures/mole_hunter/mole_hunted.png", sizeFactor, sizeFactor, Button.RE);
        moles.put(Button.RE, current);
        allForegroundActors.add(current);
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_foreground.png", sizeFactor, sizeFactor, Button.RE));
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_background.png", sizeFactor, sizeFactor, Button.MI));
        current = new MoleActor(controller.getAssetsManager(), "textures/mole_hunter/mole.png", "textures/mole_hunter/mole_hunted.png", sizeFactor, sizeFactor, Button.MI);
        moles.put(Button.MI, current);
        allForegroundActors.add(current);
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_foreground.png", sizeFactor, sizeFactor, Button.MI));
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_background.png", sizeFactor, sizeFactor, Button.FA));
        current = new MoleActor(controller.getAssetsManager(), "textures/mole_hunter/mole.png", "textures/mole_hunter/mole_hunted.png", sizeFactor, sizeFactor, Button.FA);
        moles.put(Button.FA, current);
        allForegroundActors.add(current);
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_foreground.png", sizeFactor, sizeFactor, Button.FA));
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_background.png", sizeFactor, sizeFactor, Button.A));
        current = new MoleActor(controller.getAssetsManager(), "textures/mole_hunter/mole.png", "textures/mole_hunter/mole_hunted.png", sizeFactor, sizeFactor, Button.A);
        moles.put(Button.A, current);
        allForegroundActors.add(current);
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_foreground.png", sizeFactor, sizeFactor, Button.A));
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_background.png", sizeFactor, sizeFactor, Button.B));
        current = new MoleActor(controller.getAssetsManager(), "textures/mole_hunter/mole.png", "textures/mole_hunter/mole_hunted.png", sizeFactor, sizeFactor, Button.B);
        moles.put(Button.B, current);
        allForegroundActors.add(current);
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_foreground.png", sizeFactor, sizeFactor, Button.B));
        return moleActors;
    }
    protected List<ActorDisposable> createRightMoles() {
        List<ActorDisposable> moleActors = new ArrayList();
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_background.png", sizeFactor, sizeFactor, Button.SOL));
        MoleActor current = new MoleActor(controller.getAssetsManager(), "textures/mole_hunter/mole.png", "textures/mole_hunter/mole_hunted.png", sizeFactor, sizeFactor, Button.SOL);
        moles.put(Button.SOL, current);
        allForegroundActors.add(current);
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_foreground.png", sizeFactor, sizeFactor, Button.SOL));
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_background.png", sizeFactor, sizeFactor, Button.LA));
        current = new MoleActor(controller.getAssetsManager(), "textures/mole_hunter/mole.png", "textures/mole_hunter/mole_hunted.png", sizeFactor, sizeFactor, Button.LA);
        moles.put(Button.LA, current);
        allForegroundActors.add(current);
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_foreground.png", sizeFactor, sizeFactor, Button.LA));
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_background.png", sizeFactor, sizeFactor, Button.SI));
        current = new MoleActor(controller.getAssetsManager(), "textures/mole_hunter/mole.png", "textures/mole_hunter/mole_hunted.png", sizeFactor, sizeFactor, Button.SI);
        moles.put(Button.SI, current);
        allForegroundActors.add(current);
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_foreground.png", sizeFactor, sizeFactor, Button.SI));
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_background.png", sizeFactor, sizeFactor, Button.DO_PRIME));
        current = new MoleActor(controller.getAssetsManager(), "textures/mole_hunter/mole.png", "textures/mole_hunter/mole_hunted.png", sizeFactor, sizeFactor, Button.DO_PRIME);
        moles.put(Button.DO_PRIME, current);
        allForegroundActors.add(current);
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_foreground.png", sizeFactor, sizeFactor, Button.DO_PRIME));
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_background.png", sizeFactor, sizeFactor, Button.C));
        current = new MoleActor(controller.getAssetsManager(), "textures/mole_hunter/mole.png", "textures/mole_hunter/mole_hunted.png", sizeFactor, sizeFactor, Button.C);
        moles.put(Button.C, current);
        allForegroundActors.add(current);
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_foreground.png", sizeFactor, sizeFactor, Button.C));
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_background.png", sizeFactor, sizeFactor, Button.D));
        current = new MoleActor(controller.getAssetsManager(), "textures/mole_hunter/mole.png", "textures/mole_hunter/mole_hunted.png", sizeFactor, sizeFactor, Button.D);
        moles.put(Button.D, current);
        allForegroundActors.add(current);
        allForegroundActors.add(new TextureButtonPositionActor(controller.getAssetsManager(), "textures/mole_hunter/mole_foreground.png", sizeFactor, sizeFactor, Button.D));
        return moleActors;
    }

    public void punchMole(Button position) {
        moles.get(position).punchMole();
    }

    public void countdown(int countdownNumber) {
        if (countdownNumber >= 0) {
            initPopUp.changeTexture(countdownNumber%2, countdownNumber/2);
        } else {
            initPopUp.setVisible(false);
            initPopUp.dispose();
        }
    }

    public abstract void updatePlayerTime(float[] secondsRemain);

    public void moleGoUp(Button position) {
        LOG.info(position.name());
        moles.get(position).showUp();
    }

    public void moleGoDown(Button position) {
        moles.get(position).showDown();
    }

    public void molePunched(Button position) {
        moles.get(position).punchMole();
    }

    public void unpunchMole(Button position) {
        moles.get(position).unpunchMole();
    }
}
