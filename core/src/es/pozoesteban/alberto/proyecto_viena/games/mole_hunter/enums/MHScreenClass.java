package es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.enums;

public enum MHScreenClass {
    MHInitialMenuScreen(0), MHOnePlayer12MolesScreen(1);

    private final int value;

    MHScreenClass(int value) {
        this.value = value;
    }
}
