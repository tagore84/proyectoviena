package es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.proyecto_viena.PrimalController;
import es.pozoesteban.alberto.proyecto_viena.actors.HorizontalRorateActor;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.BackgroundActor;
import es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.MHController;
import es.pozoesteban.alberto.proyecto_viena.screens.PrimalUIScreen;

import java.util.ArrayList;
import java.util.List;


public class MHInitialMenuScreen extends MHBaseScreen {

    private final static Logger LOG = new Logger("InitialMenuScreen");

    private List<HorizontalRorateActor> circularSlideTray;

    public MHInitialMenuScreen(MHController controller) {
        super(controller);
        disposeActors();
        initArrays();

        circularSlideTray = new ArrayList();
    }

    @Override
    public void show() {
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        stage.setDebugAll(SCREEN_DEBUG);

        createActors();
        addActorsToStage();
        loadInputs();
    }

    @Override
    protected String getBackgroudTextureName() {
        return "textures/mole_hunter/initial_menu_background.png";
    }

    @Override
    protected void createActors() {
        backgroundActor = new BackgroundActor(controller.getAssetsManager(), "textures/mole_hunter/initial_menu_background.png");
        allBackgroundActors.add(backgroundActor);

        float minX = Gdx.graphics.getWidth() * 0.01f;
        float maxX = Gdx.graphics.getWidth() * (1f - 0.01f);
        float y = Gdx.graphics.getHeight() * 0.25f;

        circularSlideTray.add(new HorizontalRorateActor(controller.getAssetsManager(), "textures/mole_hunter/one_player_12_moles_select.png", 0, minX, maxX, y, 3));
        circularSlideTray.add(new HorizontalRorateActor(controller.getAssetsManager(), "textures/mole_hunter/one_player_6_moles_select.png", 1, minX, maxX, y, 3));
        circularSlideTray.add(new HorizontalRorateActor(controller.getAssetsManager(), "textures/mole_hunter/two_players_select.png", -1, minX, maxX, y, 3));

        allForegroundActors.addAll(circularSlideTray);

    }

    public void moveLeftSlideTray() {
        ((MHController)controller).moveLeftSlieTray();
        for (HorizontalRorateActor actor : circularSlideTray) {
            actor.moveLeft();
        }
    }
    public void moveRightSlideTray() {
        ((MHController)controller).moveRightSlieTray();
        for (HorizontalRorateActor actor : circularSlideTray) {
            actor.moveRight();
        }
    }


}
