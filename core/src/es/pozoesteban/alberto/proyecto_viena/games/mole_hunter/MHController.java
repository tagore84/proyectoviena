package es.pozoesteban.alberto.proyecto_viena.games.mole_hunter;

import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.Timer;
import es.pozoesteban.alberto.proyecto_viena.GameController;
import es.pozoesteban.alberto.proyecto_viena.MainGame;
import es.pozoesteban.alberto.proyecto_viena.enums.Button;
import es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.enums.MHModes;
import es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.enums.MHScreenClass;
import es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.screens.MHInitialMenuScreen;
import es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.screens.MHOnePlayer12MolesScreen;
import es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.screens.MHPlayingScreen;

import java.util.HashMap;
import java.util.Map;

import static es.pozoesteban.alberto.proyecto_viena.games.mole_hunter.MHConfiguration.*;

public class MHController extends GameController {

    private static Logger LOG = new Logger("MHController", Logger.INFO);

    private int countdownNumber;

    private boolean[] invalidControls; // Player 0, Player 1
    private float[] playerTime;
    private int numPlayers;
    private int[] molesHunted;
    private int currentMode;

    private int currentRound;
    private Button[][] onePlayersPatron;
    private Button[][][] twoPlayersPatron;
    private Map<Button, MoleTimer> molesMap;

    private Timer initialTimer;
    private Timer mistakePenaltyTimer;
    private Timer moleUpTimer;
    private Timer playerTimeTimer;

    public MHController(MainGame game) {
        super(game);

    }

    private Timer.Task intialCutdown = new Timer.Task() {
        @Override
        public void run() {
            countdownNumber--;
            ((MHPlayingScreen)game.getScreen()).countdown(countdownNumber);
            if (countdownNumber == 0) {
                LOG.info("GO!");
                invalidControls = new boolean[]{false, false};
                playerTime = new float[]{PLAYER_TIME, PLAYER_TIME};
                playerTimeTimer.schedule(playerTimeTask, 1f, 1f, 10000);
                moleUpTimer.scheduleTask(moleGoUp, MOLE_MAX_HIDE_TIME, 0, 0);
            }
        }
    };
    private Timer.Task moleGoUp = new Timer.Task() {
        @Override
        public void run() {
            Button[] moles = new Button[4];
            if (numPlayers == 1) {
                moles[0] = onePlayersPatron[currentRound][0];
                if (onePlayersPatron[currentRound][1] != null) moles[1] = onePlayersPatron[currentRound][1];
            } else {
                moles[0] = twoPlayersPatron[0][currentRound][0];
                if (twoPlayersPatron[0][currentRound][1] != null) moles[1] = twoPlayersPatron[0][currentRound][1];
                moles[2] = twoPlayersPatron[1][currentRound][0];
                if (twoPlayersPatron[1][currentRound][1] != null) moles[3] = twoPlayersPatron[1][currentRound][1];
            }
            for (int i = 0; i < moles.length; i++) {
                if (moles[i] != null) {
                    molesMap.put(moles[i], new MoleTimer((MHPlayingScreen)game.getScreen(), moles[i]));
                }
            }
            currentRound++;
            moleUpTimer.schedule(moleGoUp, MOLE_MIN_HIDE_TIME + (DICE.nextFloat() * (MOLE_MAX_HIDE_TIME - MOLE_MIN_HIDE_TIME)), 0, 0);
        }
    };

    private Timer.Task playerTimeTask = new Timer.Task() {
        @Override
        public void run() {
            playerTime[0]--;
            if (numPlayers == 2) playerTime[1]--;
            ((MHPlayingScreen)game.getScreen()).updatePlayerTime(playerTime);
            if (playerTime[0] < 0) {
                if (numPlayers == 2) {
                    invalidControls[0] = true;
                } else if(numPlayers == 1) {
                    invalidControls = new boolean[]{true, true};
                }
            }
            if (numPlayers == 2 && playerTime[1] < 0) {
                invalidControls[1] = true;
            }
            if (playerTime[0] < 0 && (numPlayers == 1 || playerTime[1] < 0)) {
                Timer.instance().clear();
                // ToDo finish.play();
                LOG.info(("Player One punchs " + molesHunted[0] + " moles!!! Player Two punchs " + molesHunted[1] + " nmoles!!!"));
                // ToDo ((MHPlayingScreen)game.getScreen()).finishGame(molesHunted);
            }
        }
    };


    private Button[][] createRandomPatron(boolean allButtons) {
        Button[][] patron = new Button[100][2];
        for (int i = 0; i < patron.length; i++) {
            patron[i][0] = Button.values()[DICE.nextInt(12) + 1];
            if (DICE.nextFloat() <= DOUBLE_MOLE_LIKELIHOOD) {
                do {
                    patron[i][1] = Button.values()[DICE.nextInt(12) + 1];
                } while (patron[i][1] == patron[i][0]);
            }
        }
        return patron;
    }

    private void punchMole(Button punched) {
        final int player = (punched.getValue() > 5 && numPlayers == 2) ? 1 : 0;
        if (molesMap.containsKey(punched) && molesMap.get(punched).isUp()) {
            LOG.info("PUNCHED!");
            molesMap.get(punched).isUp(false);
            ((MHPlayingScreen)game.getScreen()).punchMole(punched);
            molesHunted[player]++;
        } else {
            LOG.info("FAKE!");
            invalidControls[player] = true;
            mistakePenaltyTimer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    invalidControls[player] = false;
                }}, PENALTY_MISTAKE_TIME, 0, 0);
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Controlls">
    @Override
    public boolean buttonDoDown() {
        super.buttonDoDown();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                return true;
            case MHOnePlayer12MolesScreen:
                if (invalidControls[0]) return false;
                punchMole(Button.DO);
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    @Override
    public boolean buttonReDown() {
        super.buttonReDown();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                return true;
            case MHOnePlayer12MolesScreen:
                if (invalidControls[0]) return false;
                punchMole(Button.RE);
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    @Override
    public boolean buttonMiDown() {
        super.buttonMiDown();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                return true;
            case MHOnePlayer12MolesScreen:
                if (invalidControls[0]) return false;
                punchMole(Button.MI);
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    @Override
    public boolean buttonFaDown() {
        super.buttonFaDown();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                return true;
            case MHOnePlayer12MolesScreen:
                if (invalidControls[0]) return false;
                punchMole(Button.FA);
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    @Override
    public boolean buttonSolDown() {
        super.buttonSolDown();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                return true;
            case MHOnePlayer12MolesScreen:
                if (invalidControls[1] || (numPlayers == 1 && invalidControls[0])) return false;
                punchMole(Button.SOL);
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    @Override
    public boolean buttonLaDown() {
        super.buttonLaDown();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                return true;
            case MHOnePlayer12MolesScreen:
                if (invalidControls[1] || (numPlayers == 1 && invalidControls[0])) return false;
                punchMole(Button.LA);
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    @Override
    public boolean buttonSiDown() {
        super.buttonSiDown();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                return true;
            case MHOnePlayer12MolesScreen:
                if (invalidControls[1] || (numPlayers == 1 && invalidControls[0])) return false;
                punchMole(Button.SI);
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    @Override
    public boolean buttonDoPrimeDown() {
        super.buttonDoPrimeDown();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                return true;
            case MHOnePlayer12MolesScreen:
                if (invalidControls[1] || (numPlayers == 1 && invalidControls[0])) return false;
                punchMole(Button.DO_PRIME);
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    @Override
    public boolean buttonADown() {
        super.buttonADown();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                return true;
            case MHOnePlayer12MolesScreen:
                if (invalidControls[0]) return false;
                punchMole(Button.A);
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    @Override
    public boolean buttonBDown() {
        super.buttonBDown();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                return true;
            case MHOnePlayer12MolesScreen:
                if (invalidControls[0]) return false;
                punchMole(Button.B);
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    @Override
    public boolean buttonCDown() {
        super.buttonCDown();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                return true;
            case MHOnePlayer12MolesScreen:
                if (invalidControls[1] || (numPlayers == 1 && invalidControls[0])) return false;
                punchMole(Button.C);
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    @Override
    public boolean buttonDDown() {
        super.buttonDDown();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                return true;
            case MHOnePlayer12MolesScreen:
                if (invalidControls[1] || (numPlayers == 1 && invalidControls[0])) return false;
                punchMole(Button.D);
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    @Override
    public boolean buttonExitUp() {
        super.buttonExitUp();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                backToPrimalUI();
                return true;
            case MHOnePlayer12MolesScreen:
                backToMenu();
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    @Override
    public boolean buttonDoUp() {
        super.buttonDoUp();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                loadCurrentMode();
                return true;
            case MHOnePlayer12MolesScreen:
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    @Override
    public boolean buttonReUp() {
        super.buttonReUp();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                loadCurrentMode();
                return true;
            case MHOnePlayer12MolesScreen:
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    @Override
    public boolean buttonMiUp() {
        super.buttonMiUp();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                loadCurrentMode();
                return true;
            case MHOnePlayer12MolesScreen:
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    @Override
    public boolean buttonFaUp() {
        super.buttonFaUp();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                loadCurrentMode();
                return true;
            case MHOnePlayer12MolesScreen:
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    @Override
    public boolean buttonSolUp() {
        super.buttonSolUp();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                loadCurrentMode();
                return true;
            case MHOnePlayer12MolesScreen:
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    @Override
    public boolean buttonLaUp() {
        super.buttonLaUp();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                loadCurrentMode();
                return true;
            case MHOnePlayer12MolesScreen:
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    @Override
    public boolean buttonSiUp() {
        super.buttonSiUp();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                loadCurrentMode();
                return true;
            case MHOnePlayer12MolesScreen:
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    @Override
    public boolean buttonDoPrimeUp() {
        super.buttonDoPrimeUp();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                loadCurrentMode();
                return true;
            case MHOnePlayer12MolesScreen:
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    public boolean buttonAUp() {
        super.buttonAUp();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                ((MHInitialMenuScreen)game.getScreen()).moveLeftSlideTray();
                return true;
            case MHOnePlayer12MolesScreen:
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }

    @Override
    public boolean buttonBUp() {
        super.buttonBUp();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                ((MHInitialMenuScreen)game.getScreen()).moveRightSlideTray();
                return true;
            case MHOnePlayer12MolesScreen:
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    public boolean buttonCUp() {
        super.buttonCUp();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                ((MHInitialMenuScreen)game.getScreen()).moveLeftSlideTray();
                return true;
            case MHOnePlayer12MolesScreen:
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }

    @Override
    public boolean buttonDUp() {
        super.buttonDUp();
        switch (MHScreenClass.valueOf(game.getScreen().getClass().getSimpleName())) {
            case MHInitialMenuScreen:
                ((MHInitialMenuScreen)game.getScreen()).moveRightSlideTray();
                return true;
            case MHOnePlayer12MolesScreen:
                return true;
            default:
                LOG.error("Unknown screen! " + game.getScreen().getClass().getSimpleName());
                return false;
        }
    }
    //</editor-fold>>

    public void moveRightSlieTray() {
        currentMode--;
        if (currentMode < 0) currentMode = MHModes.values().length - 1;
        LOG.info(MHModes.values()[currentMode].name());
    }
    public void moveLeftSlieTray() {
        currentMode++;
        if (currentMode >= MHModes.values().length)  currentMode = 0;
        LOG.info(MHModes.values()[currentMode].name());
    }

    public void loadCurrentMode() {
        MHModes mode = MHModes.values()[currentMode];
        switch (mode) {
            case ONE_PLAYER_12_MOLES:
                goToOnePlayer12MolesMode();
                return;
            case ONE_PLAYER_6_MOLES:
                goToOnePlayer6MolesMode();
                return;
            case TWO_PLAYERS:
                goToTwoPlayersMode();
                return;
            default:
                LOG.error("Mode " + mode.name() + " not implemented yet");
        }
    }

    private void goToOnePlayer6MolesMode() {
        LOG.error(" ToDo goToOnePlayer6MolesMode NOT IMPLEMENTED YET");
    }

    private void goToTwoPlayersMode() {
        LOG.error(" ToDo goToTwoPlayersMode NOT IMPLEMENTED YET");
    }

    public void backToMenu() {
        Timer.instance().clear();
        game.changeScreen(new MHInitialMenuScreen(this));
    }


    private void goToOnePlayer12MolesMode() {
        this.onePlayersPatron = createRandomPatron(true);
        countdownNumber = 3;
        invalidControls = new boolean[]{true, true};
        numPlayers = 1;
        currentRound = 0;
        molesHunted = new int[]{0, 0};
        game.changeScreen(new MHOnePlayer12MolesScreen(this));
        molesMap = new HashMap();
        initialTimer = new Timer();
        mistakePenaltyTimer = new Timer();
        initialTimer.schedule(intialCutdown, 1f, 1.1f, 3);

        playerTimeTimer = new Timer();
        moleUpTimer = new Timer();
    }
}
