package es.pozoesteban.alberto.proyecto_viena;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.proyecto_viena.enums.GamesName;
import es.pozoesteban.alberto.proyecto_viena.screens.PrimalUIScreen;

public class PrimalController extends GameController {

    private final static Logger LOG = new Logger("Primal", Logger.INFO);

    private int currentGame;

    public PrimalController(MainGame game) {
        super(game);
        for (Controller controller : Controllers.getControllers()) {
            LOG.info("Controller detected: " + controller.getName());
        }
    }

    //<editor-fold defaultstate="collapsed" desc="Controlls">

    @Override
    public boolean buttonExitUp() {
        super.buttonExitUp();
        Gdx.app.exit();
        return true;
    }
    @Override
    public boolean buttonDoUp() {
        super.buttonDoUp();
        loadCurrentGame();
        return true;
    }
    @Override
    public boolean buttonReUp() {
        super.buttonReUp();
        loadCurrentGame();
        return true;
    }
    @Override
    public boolean buttonMiUp() {
        super.buttonMiUp();
        loadCurrentGame();
        return true;
    }
    @Override
    public boolean buttonFaUp() {
        super.buttonFaUp();
        loadCurrentGame();
        return true;
    }
    @Override
    public boolean buttonSolUp() {
        super.buttonSolUp();
        loadCurrentGame();
        return true;
    }
    @Override
    public boolean buttonLaUp() {
        super.buttonLaUp();
        loadCurrentGame();
        return true;
    }
    @Override
    public boolean buttonSiUp() {
        super.buttonSiUp();
        loadCurrentGame();
        return true;
    }
    @Override
    public boolean buttonDoPrimeUp() {
        super.buttonDoPrimeUp();
        loadCurrentGame();
        return true;
    }

    @Override
    public boolean buttonAUp() {
        super.buttonAUp();
        ((PrimalUIScreen)game.getScreen()).moveLeftSlideTray();
        return true;
    }

    @Override
    public boolean buttonBUp() {
        super.buttonBUp();
        ((PrimalUIScreen)game.getScreen()).moveRightSlideTray();
        return true;
    }
    //</editor-fold>

    public void moveRightSlieTray() {
        currentGame--;
        if (currentGame < 0) currentGame = GamesName.values().length - 1;
    }
    public void moveLeftSlieTray() {
        currentGame++;
        if (currentGame >= GamesName.values().length)  currentGame = 0;
    }

    public void loadCurrentGame() {
        GamesName gameName = GamesName.values()[currentGame];
        switch (gameName) {
            case GRAND_PIANO_KEYS:
                game.loadGrandPianoKeysGame();
                return;
            case TIMELINE:
                game.loadTimeline();
                return;
            case MOLE_HUNTER:
                game.loadMoleHunter();
                return;
            case RANDOM_GAME:
                int random = DICE.nextInt(GamesName.values().length - 2) + 1;
                for (int i = 0; i < random; i++) {
                    ((PrimalUIScreen)game.getScreen()).moveRightSlideTray();
                }
                return;
            case SIMON:
                game.loadSimon();
                return;
            default:
                LOG.error("Game " + gameName.name() + " not implemented yet");
        }
    }

    @Override
    protected void loadSounds() {
        super.loadSounds();
        LOG.info("ToDo load sounds");
        doTone = assetsManager.getSound("sounds/gpk/do.mp3");// Gdx.audio.newSound(Gdx.files.internal("sounds/gpk/do.wav"));
    }

    @Override
    protected void disposeSounds() {
        super.disposeSounds();
        assetsManager.disposeAsset("sounds/gpk/do.mp3");
    }
}
