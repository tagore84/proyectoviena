package es.pozoesteban.alberto.proyecto_viena.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import es.pozoesteban.alberto.proyecto_viena.GameController;
import es.pozoesteban.alberto.proyecto_viena.PrimalController;
import es.pozoesteban.alberto.proyecto_viena.actors.HorizontalRorateActor;
import es.pozoesteban.alberto.proyecto_viena.games.grand_piano_keys.actors.BackgroundActor;

import java.util.ArrayList;
import java.util.List;


public class PrimalUIScreen extends SixMassiveButtonsScreen {


    private List<HorizontalRorateActor> circularSlideTray;

    public PrimalUIScreen(GameController controller) {
        super(controller);
        disposeActors();
        initArrays();
        circularSlideTray = new ArrayList();
    }

    @Override
    public void show() {
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        stage.setDebugAll(SCREEN_DEBUG);

        createActors();
        addActorsToStage();

        loadInputs();
    }

    @Override
    protected void createActors() {
        backgroundActor = new BackgroundActor(controller.getAssetsManager(), "textures/primal_ui.png");
        allBackgroundActors.add(backgroundActor);

        float minX = Gdx.graphics.getWidth() * 0.01f;
        float maxX = Gdx.graphics.getWidth() * (1f - 0.01f);
        float y = Gdx.graphics.getHeight() * 0.25f;

        circularSlideTray.add(new HorizontalRorateActor(controller.getAssetsManager(), "textures/simon/preview.png", -2, minX, maxX, y, 5));
        circularSlideTray.add(new HorizontalRorateActor(controller.getAssetsManager(), "textures/random_preview.png", -1, minX, maxX, y, 5));
        circularSlideTray.add(new HorizontalRorateActor(controller.getAssetsManager(), "textures/gpk/preview.png", 0, minX, maxX, y, 5));
        circularSlideTray.add(new HorizontalRorateActor(controller.getAssetsManager(), "textures/timeline/preview.png", 1, minX, maxX, y, 5));
        circularSlideTray.add(new HorizontalRorateActor(controller.getAssetsManager(), "textures/mole_hunter/preview.png", 2, minX, maxX, y, 5));

        allForegroundActors.addAll(circularSlideTray);
    }

    public void moveLeftSlideTray() {
        ((PrimalController)controller).moveLeftSlieTray();
        for (HorizontalRorateActor actor : circularSlideTray) {
            actor.moveLeft();
        }
    }
    public void moveRightSlideTray() {
        ((PrimalController)controller).moveRightSlieTray();
        for (HorizontalRorateActor actor : circularSlideTray) {
            actor.moveRight();
        }
    }
}
