package es.pozoesteban.alberto.proyecto_viena.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerAdapter;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.proyecto_viena.GameController;
import es.pozoesteban.alberto.proyecto_viena.actors.ActorDisposable;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

public abstract class SixMassiveButtonsScreen implements Screen {

    private final static Logger LOG = new Logger("BaseScreen");

    protected static final boolean SCREEN_DEBUG = true;

    protected GameController controller;
    protected Stage stage;

    protected ActorDisposable backgroundActor;

    protected List<ActorDisposable> allBackgroundActors;
    protected List<ActorDisposable> allForegroundActors;
    protected List<Animation<TextureRegion>> allAnimations;
    protected List<Actor> nonDisposablesActors;

    protected float elapsed;

    public SixMassiveButtonsScreen(GameController controller){
        this.controller = controller;
        elapsed = 0;
    }

    protected void initArrays() {
        allBackgroundActors = new ArrayList();
        allForegroundActors = new ArrayList();
        allAnimations = new ArrayList();
        nonDisposablesActors = new ArrayList();
    }

    public abstract void show();

    protected void loadInputs() {
        loadKeyboardInputs();
        //loadButtonsInputs(); Como se guarda el listener en el Controller en vez de en la Screen, no hay que hacerlo varias veces
    }

    private void loadKeyboardInputs() {
        stage.addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                switch (keycode) {
                    case Input.Keys.Q:
                        return controller.buttonExitDown();
                    case Input.Keys.A:
                        return controller.buttonDoDown();
                    case Input.Keys.S:
                        return controller.buttonReDown();
                    case Input.Keys.D:
                        return controller.buttonMiDown();
                    case Input.Keys.F:
                        return controller.buttonFaDown();

                    case Input.Keys.H:
                        return controller.buttonSolDown();
                    case Input.Keys.J:
                        return controller.buttonLaDown();
                    case Input.Keys.K:
                        return controller.buttonSiDown();
                    case Input.Keys.L:
                        return controller.buttonDoPrimeDown();

                    case Input.Keys.W:
                        return controller.buttonADown();
                    case Input.Keys.E:
                        return controller.buttonBDown();

                    case Input.Keys.I:
                        return controller.buttonCDown();
                    case Input.Keys.O:
                        return controller.buttonDDown();
                    default:
                        return false;
                }
            }

            public boolean keyUp(InputEvent event, int keycode) {
                switch (keycode) {
                    case Input.Keys.Q:
                        return controller.buttonExitUp();
                    case Input.Keys.A:
                        return controller.buttonDoUp();
                    case Input.Keys.S:
                        return controller.buttonReUp();
                    case Input.Keys.D:
                        return controller.buttonMiUp();
                    case Input.Keys.F:
                        return controller.buttonFaUp();

                    case Input.Keys.W:
                        return controller.buttonAUp();
                    case Input.Keys.E:
                        return controller.buttonBUp();

                    case Input.Keys.H:
                        return controller.buttonSolUp();
                    case Input.Keys.J:
                        return controller.buttonLaUp();
                    case Input.Keys.K:
                        return controller.buttonSiUp();
                    case Input.Keys.L:
                        return controller.buttonDoPrimeUp();

                    case Input.Keys.I:
                        return controller.buttonCUp();
                    case Input.Keys.O:
                        return controller.buttonDUp();
                    default:
                        return false;
                }
            }
        });
    }

    protected abstract void createActors();

    protected void addActorsToStage() {
        Group background = new Group();
        Group foreground = new Group();
        stage.addActor(background);
        stage.addActor(foreground);
        for (Actor actor : allBackgroundActors) {
            background.addActor(actor);
        }
        for (Actor actor : allForegroundActors) {
            foreground.addActor(actor);
        }
        for (Actor actor : nonDisposablesActors) {
            foreground.addActor(actor);
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        dispose();
        stage.dispose();
        Gdx.input.setInputProcessor(null);
    }


    @Override
    public void dispose() {
        disposeActors();
    }

    protected void disposeActors() {
        if (allBackgroundActors != null) {
            for (ActorDisposable actor : allBackgroundActors) {
                actor.dispose();
            }
        }
        if (allForegroundActors != null) {
            for (ActorDisposable actor : allForegroundActors) {
                actor.dispose();
            }
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        elapsed += Gdx.graphics.getDeltaTime();
        stage.getBatch().begin();
        for (Animation<TextureRegion> animation : allAnimations) {
            stage.getBatch().draw(animation.getKeyFrame(elapsed), 20.0f, 20.0f);
        }
        stage.getBatch().end();
        stage.act();
        stage.draw();


    }
}

