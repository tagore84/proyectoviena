package es.pozoesteban.alberto.proyecto_viena.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import es.pozoesteban.alberto.proyecto_viena.MainGame;

public class DesktopLauncher {
	public static void main (String[] args) {
		//windowsMode(args);
		fullScreenMode(args);
	}

	public static void fullScreenMode (String[] args) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = LwjglApplicationConfiguration.getDesktopDisplayMode().width;
		config.height = LwjglApplicationConfiguration.getDesktopDisplayMode().height;
		config.fullscreen = true;
		config.forceExit = false;

		new LwjglApplication(new MainGame(), config);
	}

	public static void windowsMode (String[] args) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.forceExit = false;

		new LwjglApplication(new MainGame(), config);
	}
}
